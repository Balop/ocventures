<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!-- Tell the browser to be responsive to screen width -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="Welcome to OC Money Fintech App. Seamless record management is the drive towards better customer service. Powered by Adlio Technologies">
    <meta name="author" content="Adlio Technologies">
    <!-- Favicon icon -->
    <link rel="icon" type="image/png" sizes="16x16" href="{{ asset('paga/images/favicon.png') }}">
    <title>OC Money Online Banking</title>

    <link href="{{ asset('paga/css/lib/chartist/chartist.min.css') }}" rel="stylesheet">
    <link href="{{ asset('paga/css/lib/owl.carousel.min.css') }}" rel="stylesheet" />
    <link href="{{ asset('paga/css/lib/owl.theme.default.min.css') }}" rel="stylesheet" />
    <!-- Bootstrap Core CSS -->
    <link href="{{ asset('paga/css/lib/bootstrap/bootstrap.min.css') }}" rel="stylesheet">
    <!-- Custom CSS -->
    <link href="{{ asset('paga/css/helper.css') }}" rel="stylesheet">
    <link href="{{ asset('paga/css/style.css') }}" rel="stylesheet">
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:** -->
    <!--[if lt IE 9]>
    <script src="https:**oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js') }}"></script>
    <script src="https:**oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js') }}"></script>
<![endif]-->
</head>

<body class="fix-header fix-sidebar">
    <!-- Preloader - style you can find in spinners.css -->
    <div class="preloader">
        <svg class="circular" viewBox="25 25 50 50">
            <circle class="path" cx="50" cy="50" r="20" fill="none" stroke-width="2" stroke-miterlimit="10" /> </svg>
    </div>
    <!-- Main wrapper  -->
    <div id="main-wrapper">
        <!-- header header  -->
        <div class="header">
            <nav class="navbar top-navbar navbar-expand-md navbar-light">
                <!-- Logo -->
                <div class="navbar-header">
                    <a class="navbar-brand" href="{{ route('home') }}">
                        <!-- Logo icon -->
                        <b>OC <i class="fa fa-bank"></i></b>
                        <!-- <b><img src="{{ asset('paga/images/logo.png') }}" alt="homepage" class="dark-logo" /></b> -->
                        <!--End Logo icon -->
                        <!-- Logo text -->
                        <span><b>Money</b></span>
                    </a>
                </div>
                <!-- End Logo -->
                <div class="navbar-collapse">
                    <!-- toggle and nav items -->
                    <ul class="navbar-nav mr-auto mt-md-0">
                        <!-- This is  -->
                        <li class="nav-item"> <a class="nav-link nav-toggler hidden-md-up text-muted  " href="javascript:void(0)"><i class="mdi mdi-menu"></i></a> </li>
                        <li class="nav-item m-l-10"> <a class="nav-link sidebartoggler hidden-sm-down text-muted  " href="javascript:void(0)"><i class="ti-menu"></i></a> </li>
                        
                    </ul>
                    <!-- User profile and search -->
                    <ul class="navbar-nav my-lg-0">

                        <!-- Search -->
                        <li class="nav-item hidden-sm-down search-box"> <a class="nav-link hidden-sm-down text-muted  " href="javascript:void(0)"><i class="ti-search"></i></a>
                            <form class="app-search">
                                <input type="text" class="form-control" placeholder="Search here"> <a class="srh-btn"><i class="ti-close"></i></a> </form>
                        </li>
                       
                        <!-- Profile -->
                        <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle text-muted  " href="#" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><img src="{{ asset('paga/images/users/5.jpg') }}" alt="user" class="profile-pic" />{{$person->username}}</a>
                            <div class="dropdown-menu dropdown-menu-right animated zoomIn">
                                <ul class="dropdown-user">
                                      <li><a href="#" data-toggle="modal" data-target="#inlineForm" class="nav-link dropdown-user-link"><span class="avatar avatar-online"><i class="fa fa-pencil"></i></span><span class="user-name"> Edit Profile <i class="icon-edit"></i></span></a>
                                    </li>
                                    <li><a href="{{ route('logout') }}"><i class="fa fa-power-off"></i> Logout</a></li>
                                </ul>
                            </div>
                        </li>
                    </ul>
                </div>
            </nav>
        </div>
        <!-- End header header -->
        <!-- Left Sidebar  -->
        <div class="left-sidebar">
            <!-- Sidebar scroll-->
            <div class="scroll-sidebar">
                <!-- Sidebar navigation-->
                <nav class="sidebar-nav">
                    <ul id="sidebarnav">
                        <li class="nav-devider"></li>
                        <li class="nav-label">Home</li>
                        <li> <a  href="{{ route('home') }}" aria-expanded="false"><i class="fa fa-tachometer"></i><span class="hide-menu">Dashboard</a>
                        </li>
                        <li class="nav-label">Transactions</li>
                        <li> <a class="has-arrow  " href="#" aria-expanded="false"><i class="fa fa-book"></i><span class="hide-menu">Withdrawal</span></a>
                            <ul aria-expanded="false" class="collapse">
                                <li><a href="{{route('new_transaction', ['Withdrawal'])}}">New</a></li>
                                <li><a href="{{route('today_history', ['Withdrawal'])}}">Today's History</a></li>
                            </ul>
                        </li>

                         <li> <a class="has-arrow  " href="#" aria-expanded="false"><i class="fa fa-envelope"></i><span class="hide-menu">Deposit</span></a>
                            <ul aria-expanded="false" class="collapse">
                                <li><a href="{{route('new_transaction', ['Deposit'])}}">New</a></li>
                                <li><a href="{{route('today_history', ['Deposit'])}}">Today's History</a></li>
                            </ul>
                        </li>

                         <li> <a class="has-arrow  " href="#" aria-expanded="false"><i class="fa fa-arrow-right"></i><span class="hide-menu">Transfer</span></a>
                            <ul aria-expanded="false" class="collapse">
                                <li><a href="{{route('new_transaction', ['Transfer'])}}">New</a></li>
                                <li><a href="{{route('today_history', ['Transfer'])}}">Today's History</a></li>
                            </ul>
                        </li>

                         <li> <a class="has-arrow  " href="#" aria-expanded="false"><i class="fa fa-television"></i><span class="hide-menu">Cables & Electricity</span></a>
                            <ul aria-expanded="false" class="collapse">
                                <li><a href="{{route('new_transaction', ['Cable'])}}">New</a></li>
                                <li><a href="{{route('today_history', ['Cable'])}}">Today's History</a></li>
                            </ul>
                        </li>

                        <li class="nav-label">General Reports</li>

                         <li> <a class="has-arrow  " href="#" aria-expanded="false"><i class="fa fa-suitcase"></i><span class="hide-menu">Report</span></a>
                            <ul aria-expanded="false" class="collapse">

                        <li> <a class="has-arrow  " href="{{route('all_today_history') }}" aria-expanded="false"><span class="hide-menu">Todays' Report <span class="label label-rouded label-warning pull-right"></span></span></a>
                        </li>

                        <li> <a class="has-arrow  " href="{{route('all_history') }}" aria-expanded="false"></i><span class="hide-menu">All Time Report <span class="label label-rouded label-danger pull-right"></span></span></a>
                        </li>

                        </ul>
                        </li>

                        @if(Auth::User()->user_role == 1)
                        <li> <a  href="{{ route('trash') }}" aria-expanded="false"><i class="fa fa-trash"></i><span class="hide-menu">Trash</a>
                        </li>
                        @endif

                        <li class="nav-label">EXTRA</li>
                        @if(Auth::User()->user_role == 1)
                         <li> <a class="has-arrow  " href="#" aria-expanded="false"><i class="fa fa-user"></i><span class="hide-menu">User Setup</span></a>
                            <ul aria-expanded="false" class="collapse">
                        <li> <a  href="{{ route('join') }}" aria-expanded="false"><i class="fa fa-form"></i><span class="hide-menu">Create New User</span></a>
                        </li>

                        <li> <a  href="{{ route('user_list') }}" aria-expanded="false"><i class="fa fa-grid"></i><span class="hide-menu">User List</span></a>
                        </li>
                    </ul>
                    </li>

                        @endif
                        <li> <a  href="{{ route('logout') }}" aria-expanded="false"><i class="fa fa-power-off"></i><span class="hide-menu">Logout</a>
                        </li>
                    </ul>
                </nav>
                <!-- End Sidebar navigation -->
            </div>
            <!-- End Sidebar scroll-->
        </div>
        <!-- End Left Sidebar  -->


        <!-- the modal below is used to edit the logged in person profile....firstly to verify -->
        <div class="modal text-xs-left" id="inlineForm" tabindex="-1" role="dialog" aria-labelledby="myModalLabel33" aria-hidden="true">
                    <div class="modal-dialog" role="document">
                      <div class="modal-content">
                        <div class="modal-header">
                          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                          </button>
                          <label class="modal-title text-text-bold-600" id="myModalLabel33"><strong>Confirm Password</strong></label>
                        </div>
                        <form action="{{ route('checkedit') }}" method="post">
                          @csrf
                            <div class="modal-body">
                            <div class="form-group" >
                                            <label for="password">Password</label>
                                            <input type="password" id="password" class="form-control @error('password') is-invalid @enderror" value="{{ old('password') }}" placeholder="password" name="password" required="required" minlength="8">

                                             @error('password')
                                    <span class="invalid-feedback" style="color:red" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                                        </div>
                                    <div>
                                      <input type="text" name="id" value="{{Auth::user()->id}}" hidden>
                                      <p class="text-center" style="margin-left: 10%">Verify your password to proceed to editing profile</p>
                                    </div>        
                          </div>
                          <div class="modal-footer">
                          <input type="submit" class="btn btn-outline-primary btn-lg" value="Verify">
                          </div>
                        </form>
                      </div>
                    </div>
                  </div>


  
        
            @yield('content')
        




            <!--Footer-->
<footer class="footer"> © 2021 All rights reserved. Designed by <a href="http://belloadebowale.tk" target="_blank">Adlio Technologies</a></footer>
            <!-- End footer -->
        </div>
        <!-- End Page wrapper  -->
    </div>
    <!-- End Wrapper -->
    <!-- All Jquery -->
    <script src="{{ asset('paga/js/lib/jquery/jquery.min.js') }}"></script>
    <!-- Bootstrap tether Core JavaScript -->
    <script src="{{ asset('paga/js/lib/bootstrap/js/popper.min.js') }}"></script>
    <script src="{{ asset('paga/js/lib/bootstrap/js/bootstrap.min.js') }}"></script>
    <!-- slimscrollbar scrollbar JavaScript -->
    <script src="{{ asset('paga/js/jquery.slimscroll.js') }}"></script>
    <!--Menu sidebar -->
    <script src="{{ asset('paga/js/sidebarmenu.js') }}"></script>
    <!--stickey kit -->
    <script src="{{ asset('paga/js/lib/sticky-kit-master/dist/sticky-kit.min.js') }}"></script>


    <script src="{{ asset('paga/js/lib/datamap/d3.min.js') }}"></script>
    <script src="{{ asset('paga/js/lib/datamap/topojson.js') }}"></script>
    <script src="{{ asset('paga/js/lib/datamap/datamaps.world.min.js') }}"></script>
    <script src="{{ asset('paga/js/lib/datamap/datamap-init.js') }}"></script>

    <script src="{{ asset('paga/js/lib/weather/jquery.simpleWeather.min.js') }}"></script>
    <script src="{{ asset('paga/js/lib/weather/weather-init.js') }}"></script>
    <script src="{{ asset('paga/js/lib/owl-carousel/owl.carousel.min.js') }}"></script>
    <script src="{{ asset('paga/js/lib/owl-carousel/owl.carousel-init.js') }}"></script>


    <script src="{{ asset('paga/js/lib/chartist/chartist.min.js') }}"></script>
    <script src="{{ asset('paga/js/lib/chartist/chartist-plugin-tooltip.min.js') }}"></script>
    <script src="{{ asset('paga/js/lib/chartist/chartist-init.js') }}"></script>
    <!--Custom JavaScript -->
    <script src="{{ asset('paga/js/custom.min.js') }}"></script>

    <script src="{{ asset('paga/js/lib/datatables/datatables.min.js') }}"></script>
    <script src="{{ asset('paga/js/lib/datatables/cdn.datatables.net/buttons/1.2.2/js/dataTables.buttons.min.js') }}"></script>
    <script src="{{ asset('paga/js/lib/datatables/cdn.datatables.net/buttons/1.2.2/js/buttons.flash.min.js') }}"></script>
    <script src="{{ asset('paga/js/lib/datatables/cdnjs.cloudflare.com/ajax/libs/jszip/2.5.0/jszip.min.js') }}"></script>
    <script src="{{ asset('paga/js/lib/datatables/cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/pdfmake.min.js') }}"></script>
    <script src="{{ asset('paga/js/lib/datatables/cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/vfs_fonts.js') }}"></script>
    <script src="{{ asset('paga/js/lib/datatables/cdn.datatables.net/buttons/1.2.2/js/buttons.html5.min.js') }}"></script>
    <script src="{{ asset('paga/js/lib/datatables/cdn.datatables.net/buttons/1.2.2/js/buttons.print.min.js') }}"></script>
    <script src="{{ asset('paga/js/lib/datatables/datatables-init.js') }}"></script>

   </body>

</html>






