@extends('layouts.pagemaster')

@section('content')
<div class="rectangle-4">
        <div class="row" style="margin-left:8%">
            <div class="col-md-2 col-sm-6 hidden-md-down" style="margin-top:2%">
                <h3 style="font-family: 'Poppins';font-size: 30px;font-weight: 700;line-height: 42px;">My Network</h3>
            </div>
            <div class="col-md-9 col-sm-6" style="margin-top:1%">
                <div style="float:right; margin-right:10%">
                    <nav class="navbar navbar-expand-sm">
                        <ul class="navbar-nav">
                            <li class="nav-item dropdown">
                                <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    <img src="{{asset('references/img/bg/user_bg.png')}}" width="40" height="40" class="rounded-circle">
                                    <span>{{$persons->username}}</span>
                                </a>
                                <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink" style="z-index:1000; position:relative">
                                    <a class="dropdown-item" href="/home">Dashboard</a>
                                    <a class="dropdown-item" href="/edit_profile">Edit Profile</a>
                                    <a class="dropdown-item" href="/logout">Log Out</a>
                                </div>
                            </li>
                        </ul>
                    </nav>
                </div>
            </div>
        </div>
    </div>

    <div class="rectangle-6 hidden-md-down">
        <div class="row" style="margin-left:8%">
            <div class="col-md-12 col-sm-6" style="margin-top:5%">
                <h4 style="color:white;font-weight:800">  My Network Role</h4>
            </div>
        </div>
    </div>

    <div class="row">
        <!--Menu-->
       <div class="col-md-2 col-sm-12" style="float:left">
                    <div class="col-md-12">
                        <button class="mytab" onclick="window.location ='{{ route("home") }}'" name="tab1">Dashboard</button>
                    </div>
                    @if(Auth::user()->profile_complete == 'no')
                    <div class="col-md-12">
                        <button class="mytab" onclick="window.location ='{{ route("profile") }}'" name="tab2">My Profile</button>
                    </div>
                    @else
                    <div class="col-md-12">
                        <button class="mytab" onclick="window.location ='{{ route("edit_profile") }}'" name="tab2">My Profile</button>
                    </div>
                    @endif
                    <div class="col-md-12">
                        <button class="mytab active" onclick="window.location ='{{ route("network") }}'" name="tab3">My Networks</button>
                    </div>
                    <div class="col-md-12">
                        <button class="mytab" onclick="window.location ='{{ route("logout") }}'" name="tab4">logout</button>
                    </div>
                </div>

        <div class="col-md-10 col-sm-12" style="float:left; background:#E0E0E0;max-width: 1063px; overflow-x:hidden">

            <!--First Tier-->
            <div id="FirstTier">
                <div class="col-md-12" style="float:left">
                    <div class="card">
                        <div class="card-body">
                            <div class="card-block">
                                <div class="media">
                                    <div class="media-body text-xs-left">
                                        <h3>First Tier Branch</h3>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-12">
                	@foreach($current_networks as $key=> $current_network)
                    <div class="col-lg-4 col-md-6" style="float:left">
                        <div class="card c_grid c_yellow">
                            <div class="body text-center">
                                <div class="circle">
                                    <img class="rounded-circle" src="{{asset('references/img/icons/user.png')}}" alt="">
                                </div>
                                <h3 style="color:#3F32D8; font-size:small">{{$current_network->fname}} {{$current_network->lname}}</h3>
                                <span style="color:#000; font-size:small;">{{$current_network->user_id}}</span>
                                <hr />
                                <div class="col-md-6" style="float:left">
                                    <div class="circle_sm">
                                        <img class="rounded-circle" src="{{asset('references/img/icons/user.png')}}" alt="">
                                    </div>
                                </div>

                                <div class="col-md-6" style="float:left;margin-top: -15%;margin-left: 10%;">
                                    @if($current_network->state == 'Active')
                                    <span style="color:#808080;font-size:14px; margin-right:20%"><label class="ellipse-2"></label><span>&nbsp;&nbsp;&nbsp;Active</span>
                                    @else
                                    <span style="color:#808080;font-size:14px; margin-right:20%"><label class="ellipse-3"></label><span>&nbsp;&nbsp;&nbsp;Offline</span>
                                    @endif
                                </div>

                                <div style="float:left" class="col-md-12">

                                </div>

                            </div>
                        </div>
                    </div>
                    @endforeach
                </div>
            </div>
            <!--Second Tier-->
            <div id="SecondTier">
                <div class="col-md-12" style="float:left">
                    <div class="card">
                        <div class="card-body">
                            <div class="card-block">
                                <div class="media">
                                    <div class="media-body text-xs-left">
                                        <h3>Second Tier Branch</h3>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-12">
                	@foreach($networks as $key=> $network)
                    <div class="col-lg-3 col-md-6" style="float:left">
                        <div class="card c_grid c_yellow">
                            <div class="body text-center">
                                <div class="circle">
                                    <img class="rounded-circle" src="{{asset('references/img/icons/user.png')}}" alt="">
                                </div>
                                <h3 style="color:#3F32D8; font-size:small">{{$network->fname}} {{$network->lname}}</h3>
                                 <span style="color:#000; font-size:small;">{{$network->user_id}}</span>
                                <hr />
                                <div class="col-md-6" style="float:left">
                                    <div class="circle_sm">
                                        <img class="rounded-circle" src="{{asset('references/img/icons/user.png')}}" alt="">
                                    </div>
                                </div>

                                <div class="col-md-6" style="float:left;margin-top: -15%;margin-left: 10%;">
                                    @if($network->state == 'Active')
                                    <span style="color:#808080;font-size:14px; margin-right:20%"><label class="ellipse-2"></label><span>&nbsp;&nbsp;&nbsp;Active</span>
                                    @else
                                    <span style="color:#808080;font-size:14px; margin-right:20%"><label class="ellipse-3"></label><span>&nbsp;&nbsp;&nbsp;Offline</span>
                                    @endif
                                </div>

                                <div style="float:left" class="col-md-12">

                                </div>

                            </div>
                        </div>
                    </div>
                    @endforeach
                </div>
                <div class="clearfix">
                </div>
            </div>
        </div>
    </div>
    <div class="clearfix"><br /><br /><br /></div>

@endsection