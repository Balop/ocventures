@extends('layouts.pagemaster')

@section('content')
<div class="page-wrapper">
<!-- Bread crumb -->
                @if (\Session::has('success'))
                
                <div class="alert alert-success col-md-12"  style="float:right" id="success_btn" >
                                <p>{!! \Session::get('success') !!}
                            <button type="submit" class="btn btn-primary mr-1" style="float:right" onclick="document.getElementById('success_btn').style.display = 'none'">
                                <i class="icon-check2"></i> Close</button></p>
                        </div>
                @endif
            <div class="row page-titles">
                <div class="col-md-5 align-self-center">
                    <h3 class="text-primary">User Setup</h3> </div>
            </div>
            <!-- End Bread crumb -->
             <!-- Container fluid  -->
            <div class="container-fluid">
                <!-- Start Page Content -->
                <!-- /# row -->
               <div class="row">
                    <div class="col-lg-12">
                        <div class="card">
                            <div class="card-title">
                                <h4>New User Information</h4>

                            </div>
                            <div class="card-body">
                                <div class="basic-elements">
                                    <form class="form" method="POST" action="{{ route('reg') }}">
                                    @csrf
                                        <div class="row">
                                            <div class="col-lg-12">

                                                <div class="form-group">
                                                    <input type="text" id="name" name="name" class="form-control @error('name') is-invalid @enderror" value="{{ old('name') }}" placeholder="Enter Fullname">

                                                     @error('name')
                                                        <span class="invalid-feedback" style="color:red" role="alert">
                                                            <strong>{{ $message }}</strong>
                                                        </span>
                                                    @enderror
                                                </div>

                                                <div class="form-group">
                                                    <input type="text" id="username" name="username" class="form-control @error('username') is-invalid @enderror" value="{{ old('username') }}" placeholder="Enter Username">

                                                     @error('username')
                                                        <span class="invalid-feedback" style="color:red" role="alert">
                                                            <strong>{{ $message }}</strong>
                                                        </span>
                                                    @enderror
                                                </div>

                                                <div class="form-group">
                                                    <input type="email" id="email" name="email" class="form-control @error('email') is-invalid @enderror" value="{{ old('email') }}" placeholder="Enter email">

                                                     @error('email')
                                                        <span class="invalid-feedback" style="color:red" role="alert">
                                                            <strong>{{ $message }}</strong>
                                                        </span>
                                                    @enderror
                                                </div>

                                                <div class="form-group">
                                                    <input type="tel" id="phone" name="phone" class="form-control @error('phone') is-invalid @enderror" value="{{ old('phone') }}" placeholder="Enter phone">

                                                     @error('phone')
                                                        <span class="invalid-feedback" style="color:red" role="alert">
                                                            <strong>{{ $message }}</strong>
                                                        </span>
                                                    @enderror
                                                </div>

                                                 <div class="form-group">
                                                     <button type="submit"  class="btn btn-primary" style="margin-left:25%; width:150px">
                                                        <i class="fa fa-user"></i> Create User <i class="fa fa-arrow-right"></i>
                                                    </button>
                                                </div>
                                                
                                            </div>
                                           
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- /# column -->
                </div>
                <!-- /# row -->             
                </div>
            </div>
@endsection
