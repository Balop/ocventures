@extends('layouts.pagemaster')

@section('content')


    <div class="error-box"> <div class="back-box"> <h2>PAYMENT</h2> </div><div class="error-box-text"> <h1 style="font-size:20px ">Hello! {{$persons->username}}</h1> <h3>Proceed To Payment</h3> <h4>You have successfully registered for {{$persons->plan}} plan, click on pay now to proceed to payment.</h4> <div class="mt-30"> <a href="https://payment.seope2020.com/likeminds/paynow.aspx?reference_id={{$persons->reference_id}}&email={{$persons->email}}&name={{$persons->fname}} {{$persons->lname}}&amount={{$plans->amount}}" class="dark-button button-md">Pay Now</a> </div></div></div>



@endsection
