<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!-- Tell the browser to be responsive to screen width -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="Welcome to OC Money Fintech App. Seamless record management is the drive towards better customer service. Powered by Adlio Technologies">
    <meta name="author" content="Adlio Technologies">
    <!-- Favicon icon -->
    <link rel="icon" type="image/png" sizes="16x16" href="{{ asset('paga/images/favicon.png') }}">
    <title>OC Money Online Banking</title>

    <link href="{{ asset('paga/css/lib/chartist/chartist.min.css') }}" rel="stylesheet">
    <link href="{{ asset('paga/css/lib/owl.carousel.min.css') }}" rel="stylesheet" />
    <link href="{{ asset('paga/css/lib/owl.theme.default.min.css') }}" rel="stylesheet" />
    <!-- Bootstrap Core CSS -->
    <link href="{{ asset('paga/css/lib/bootstrap/bootstrap.min.css') }}" rel="stylesheet">
    <!-- Custom CSS -->
    <link href="{{ asset('paga/css/helper.css') }}" rel="stylesheet">
    <link href="{{ asset('paga/css/style.css') }}" rel="stylesheet">
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:** -->
    <!--[if lt IE 9]>
    <script src="https:**oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js') }}"></script>
    <script src="https:**oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js') }}"></script>
<![endif]-->
</head>

<body class="fix-header fix-sidebar">
    <!-- Preloader - style you can find in spinners.css -->
    <div class="preloader">
        <svg class="circular" viewBox="25 25 50 50">
			<circle class="path" cx="50" cy="50" r="20" fill="none" stroke-width="2" stroke-miterlimit="10" /> </svg>
    </div>
    <!-- Main wrapper  -->
    <div class="error-page" id="wrapper">
        <div class="error-box">
            <div class="error-body text-center">
                <h1>401</h1>
                <h3 class="text-uppercase">User Access Denied </h3>
                <p class="text-muted m-t-30 m-b-30">Please contact the Admin for more information</p>
                <a class="btn btn-info btn-rounded waves-effect waves-light m-b-40" href="{{ route('logout') }}"><i class="fa fa-arrow-left"></i>  Back to login</a> </div>
            <footer class="footer text-center">&copy; 2021 All rights reserved. Designed by <a href="http://belloadebowale.tk" target="_blank">Adlio Technologies</a></footer>
        </div>
    </div>
    <!-- End Wrapper -->
    <!-- All Jquery -->
<script src="{{ asset('paga/js/lib/jquery/jquery.min.js') }}"></script>
    <!-- Bootstrap tether Core JavaScript -->
    <script src="{{ asset('paga/js/lib/bootstrap/js/popper.min.js') }}"></script>
    <script src="{{ asset('paga/js/lib/bootstrap/js/bootstrap.min.js') }}"></script>
    <!-- slimscrollbar scrollbar JavaScript -->
    <script src="{{ asset('paga/js/jquery.slimscroll.js') }}"></script>
    <!--Menu sidebar -->
    <script src="{{ asset('paga/js/sidebarmenu.js') }}"></script>
    <!--stickey kit -->
    <script src="{{ asset('paga/js/lib/sticky-kit-master/dist/sticky-kit.min.js') }}"></script>


    <script src="{{ asset('paga/js/lib/datamap/d3.min.js') }}"></script>
    <script src="{{ asset('paga/js/lib/datamap/topojson.js') }}"></script>
    <script src="{{ asset('paga/js/lib/datamap/datamaps.world.min.js') }}"></script>
    <script src="{{ asset('paga/js/lib/datamap/datamap-init.js') }}"></script>

    <script src="{{ asset('paga/js/lib/weather/jquery.simpleWeather.min.js') }}"></script>
    <script src="{{ asset('paga/js/lib/weather/weather-init.js') }}"></script>
    <script src="{{ asset('paga/js/lib/owl-carousel/owl.carousel.min.js') }}"></script>
    <script src="{{ asset('paga/js/lib/owl-carousel/owl.carousel-init.js') }}"></script>


    <script src="{{ asset('paga/js/lib/chartist/chartist.min.js') }}"></script>
    <script src="{{ asset('paga/js/lib/chartist/chartist-plugin-tooltip.min.js') }}"></script>
    <script src="{{ asset('paga/js/lib/chartist/chartist-init.js') }}"></script>
    <!--Custom JavaScript -->
    <script src="{{ asset('paga/js/custom.min.js') }}"></script>

    <script src="{{ asset('paga/js/lib/datatables/datatables.min.js') }}"></script>
    <script src="{{ asset('paga/js/lib/datatables/cdn.datatables.net/buttons/1.2.2/js/dataTables.buttons.min.js') }}"></script>
    <script src="{{ asset('paga/js/lib/datatables/cdn.datatables.net/buttons/1.2.2/js/buttons.flash.min.js') }}"></script>
    <script src="{{ asset('paga/js/lib/datatables/cdnjs.cloudflare.com/ajax/libs/jszip/2.5.0/jszip.min.js') }}"></script>
    <script src="{{ asset('paga/js/lib/datatables/cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/pdfmake.min.js') }}"></script>
    <script src="{{ asset('paga/js/lib/datatables/cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/vfs_fonts.js') }}"></script>
    <script src="{{ asset('paga/js/lib/datatables/cdn.datatables.net/buttons/1.2.2/js/buttons.html5.min.js') }}"></script>
    <script src="{{ asset('paga/js/lib/datatables/cdn.datatables.net/buttons/1.2.2/js/buttons.print.min.js') }}"></script>
    <script src="{{ asset('paga/js/lib/datatables/datatables-init.js') }}"></script>


</body>

</html>