@extends('layouts.pagemaster')

@section('content')
<div class="page-wrapper">
<!-- Bread crumb -->
            <div class="row page-titles">
                <div class="col-md-5 align-self-center">
                    <h3 class="text-primary">All Time's Transaction History</h3> </div>
            </div>
            <!-- End Bread crumb -->
             <!-- Container fluid  -->
            <div class="container-fluid">
                <!-- Start Page Content -->
                <div class="row">
                    <div class="col-12">

                        @if (\Session::has('deleted'))
                <div class="row">
                    <div class="alert alert-danger col-md-12"  style="float:right" id="success_btn" >
                        <p>{!! \Session::get('deleted') !!}</p>
                            <button type="submit" class="btn btn-primary mr-1" onclick="document.getElementById('success_btn').style.display = 'none'">
                                    <i class="icon-check2"></i> Close
                            </button>
                    </div>
                </div>
            @endif
            
                        <div class="card">
                            <div class="card-body">
                                <h4 class="card-title">Record View</h4>
                                <h6 class="card-subtitle">The report below is of all transactions ever </h6>
                                <div class="table-responsive m-t-10">
                                    <table id="example23" class="display nowrap table table-hover table-striped table-bordered" cellspacing="0" width="100%">
                                         <thead>
                                            <tr>
                                               
                                                <th>S/N</th>
                                                <th class="text-center" style="font-size: 10px;">Customer</th>
                                                <th class="text-center" style="font-size: 10px;">Amount</th>
                                                <th class="text-center" style="font-size: 10px;">Charges</th>
                                                <th class="text-center" style="font-size: 10px;">Profit</th>
                                                <th class="text-center" style="font-size: 10px;">Type</th>
                                                @if(Auth::User()->user_role == 1)
                                                <th class="text-center" style="font-size: 10px;">Posted By</th>
                                                @endif
                                                <th class="text-center" style="font-size: 10px;">Time</th>

                                                @if(Auth::User()->user_role == 1)
                                                <th class="text-center" style="font-size: 10px;">Action</th>
                                                @endif
                                            </tr>
                                           
                                        </thead>
                                        
                                        <tbody>
                                            @foreach($transactions as $key=> $transaction)
                                            <tr>
                                                <td class="text-center">{{ $key + 1 }}</td>
                                                <td class="text-center" style="font-size: 10px;">{{ $transaction->client_name }}</td>
                                                <td class="text-center" style="font-size: 10px;">&#x20A6; {{ $transaction->transaction_amount }}</td>
                                                <td class="text-center" style="font-size: 10px;">&#x20A6; {{ $transaction->charges }}</td>
                                                <td class="text-center" style="font-size: 10px;">&#x20A6; {{ $transaction->profit }}</td>
                                                <td class="text-center" style="font-size: 10px;">{{ $transaction->transaction_type }}</td>

                                                
                                                @if(Auth::User()->user_role == 1)
                                                    @if($transaction->name == Auth::User()->name)
                                                        <td class="text-center" style="font-size: 10px;">You</td>
                                                    @else
                                                        <td class="text-center" style="font-size: 10px;">{{ $transaction->name }}</td>
                                                    @endif
                                                @endif

                                                <td class="text-center" style="font-size: 10px;">{{ Carbon\Carbon::parse($transaction->created_at)->format('d-m-y H:i:s') }}</td>

                                                 @if(Auth::User()->user_role == 1)
                                                        <td class="text-center" style="font-size: 10px;"><a title="Cancel Order" href="#" onclick="setCancel()" data-toggle="modal" data-target=".bs-example-modal-center{{$transaction->id}}"><i class="fa fa-trash" style="color:red;font-size:20px"></i></td>

                                                            <!-- confirm modal here -->

                                                            <div class="modal bs-example-modal-center{{$transaction->id}}" id="modal" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
                                                    <div class="modal-dialog modal-dialog-centered">
                                                        <div class="modal-content">
                                                            <div class="modal-header">
                                                                <h5 class="modal-title mt-0">ConfirmBox</h5>

                                                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                                            </div>

                                                            <div class="modal-body">                                                            <h4>Are you sure you want to delete transaction record?</h4> 
                                                            </div>

                                                            <div class="row">
                                                                <div class="col-6"><a href="{{route('deletetransaction', [$transaction->id])}}" class="btn btn-primary mb-3" style="width:70%; margin-left: 20%"> Yes</a>
                                                                </div>
                                                            <div class="col-6"><button type="button" data-dismiss="modal" aria-hidden="true" class="btn btn-danger mb-3" style="width:70%; margin-right: 20%"> No</button>
                                                            </div>
                                                        </div><!-- /.modal-content -->
                                                    </div><!-- /.modal-dialog -->
                                                <!-- /.modal -->
                                            </div>
                                                    @endif
                                            </tr>
                                            @endforeach
                                        </tbody>
                                    </table>

                                    @if($transaction_total > 0)
                                    <br><br><br><label style="font-size: 13px; color:black; border:none"><strong>Total Amount: &#x20A6; {{ number_format($transaction_total, 2) }} </strong></label>&nbsp;&nbsp;&nbsp;&nbsp;
                                    <label style="font-size: 13px; color:black; border:none"><strong>Total Profit: &#x20A6; {{ number_format($profit_total, 2) }} </strong></label>

                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- /# row -->
            </div>
        </div>


@endsection