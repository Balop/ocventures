@extends('layouts.pagemaster')

@section('content')

<div class="rectangle-4">
        <div class="row" style="margin-left:8%">
            <div class="col-md-2 col-sm-6 hidden-md-down" style="margin-top:2%">
                <h3 style="font-family: 'Poppins';font-size: 30px;font-weight: 700;line-height: 42px;">My Profile</h3>
            </div>
            <div class="col-md-9 col-sm-6" style="margin-top:1%">
                <div style="float:right; margin-right:10%">
                    <nav class="navbar navbar-expand-sm">
                        <ul class="navbar-nav">
                            <li class="nav-item dropdown">
                                <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    <img src="{{asset('references/img/bg/user_bg.png')}}" width="40" height="40" class="rounded-circle">
                                    <span>{{Auth::user()->username}}</span>
                                </a>
                                <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink" style="z-index:1000; position:relative">
                                    <a class="dropdown-item" href="/home">Dashboard</a>
                                    <a class="dropdown-item" href="/edit_profile">Edit Profile</a>
                                    <a class="dropdown-item" href="/logout">Log Out</a>
                                </div>
                            </li>
                        </ul>
                    </nav>
                </div>
            </div>
        </div>
    </div>

    <div class="rectangle-6 hidden-md-down">
        <div class="row" style="margin-left:8%">
            <div class="col-md-12 col-sm-6" style="margin-top:5%">
                @if(Auth::user()->profile_complete == 'no')
                <h4 style="color:white;font-weight:800">  Complete Profile</h4>
                @else
                <h4 style="color:white;font-weight:800">  Edit Profile</h4>
                @endif
            </div>
        </div>
    </div>

    <div class="row">
        <!--Menu-->
       <div class="col-md-2 col-sm-12" style="float:left">
                    <div class="col-md-12">
                        <button class="mytab" onclick="window.location ='{{ route("home") }}'" name="tab1">Dashboard</button>
                    </div>
                    @if(Auth::user()->profile_complete == 'no')
                    <div class="col-md-12">
                        <button class="mytab active" onclick="window.location ='{{ route("profile") }}'" name="tab2">My Profile</button>
                    </div>
                    @else
                    <div class="col-md-12">
                        <button class="mytab active" onclick="window.location ='{{ route("edit_profile") }}'" name="tab2">My Profile</button>
                    </div>
                    @endif
                    <div class="col-md-12">
                        <button class="mytab" onclick="window.location ='{{ route("network") }}'" name="tab3">My Networks</button>
                    </div>
                    <div class="col-md-12">
                        <button class="mytab" onclick="window.location ='{{ route("logout") }}'" name="tab4">logout</button>
                    </div>
                </div>

                <!---Page-->
        <div class="col-md-10 col-sm-12" style="float:left; background-color:grey; padding-left:30px; width: 420px; overflow-x:hidden">
        	
        	@if (\Session::has('success'))
                <div class="row">
                    <div class="alert alert-success col-md-4"  style="float:right" id="success_btn" >
                        <p>{!! \Session::get('success') !!}</p>
                            <button type="submit" class="btn btn-primary mr-1" onclick="document.getElementById('success_btn').style.display = 'none'">
                                    <i class="icon-check2"></i> Close
                            </button>
                    </div>
                </div>
            @endif

            <div class="contact-form-box">
                <form class="contact-form row" style="margin-top:5%" action="{{route('update_profile', $profiles->id)}}" method="post">
                	@csrf
                        <div class="col-md-6">
                            <input type="text" name="fname" class="form-control @error('fname') is-invalid @enderror" value="{{$profiles->fname}}" placeholder="First Name">
                        	@error('fname')
                                    <span class="invalid-feedback" style="color:red" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                        </div>

                        <div class="col-md-6">
                            <input type="text" name="lname" class="form-control @error('lname') is-invalid @enderror" value="{{$profiles->lname}}" placeholder="Last Name">
                        	@error('lname')
                                    <span class="invalid-feedback" style="color:red" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                        </div>

                        <div class="col-md-6">
                            <input type="text" name="email" class="form-control @error('email') is-invalid @enderror" value="{{$profiles->email}}" placeholder="email">
                        	@error('email')
                                    <span class="invalid-feedback" style="color:red" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                        </div>
                        <div class="col-md-6">
                            <input type="text" name="phone" class="form-control @error('phone') is-invalid @enderror" value="{{$profiles->phone}}" placeholder="phone">
                        	@error('phone')
                                    <span class="invalid-feedback" style="color:red" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                        </div>
                    <div class="col-md-6">
                        <select name="gender" class="form-control @error('gender') is-invalid @enderror">
                                                <option selected="" disabled="disabled">--Gender--</option>
                                                <option value="Male">Male</option>
                                                <option value="Female">Female</option>
                                            </select>

                                            @error('gender')
                                                <span class="invalid-feedback" style="color:red" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                            @enderror
                    </div>
                    <div class="col-md-6">
                       <input type="date" value="{{$profiles->dob}}" class="form-control @error('dob') is-invalid @enderror" name="dob">
                       @error('dob')
                           <span class="invalid-feedback" style="color:red" role="alert">
                               <strong>{{ $message }}</strong>
                           </span>
                       @enderror
                    </div>
                    <div class="col-md-12">
                       <textarea cols="3" rows="3"  name="address" class="form-control @error('address') is-invalid @enderror" placeholder="Address">{{$profiles->address}}</textarea>

		                       @error('address')
			                       <span class="invalid-feedback" style="color:red" role="alert">
			                       <strong>{{ $message }}</strong></span>
		                       @enderror
                    </div>

                    <div class="col-md-12">
                       <!-- <button type="submit" class="btn btn-primary text-cen" style="width: 20%; margin-left: 35%; margin-bottom: 20px; text-align: center">Save</button> -->

                       <button class="pricing-list-2-button pricing-list-2-button-act active" style="float:left; width: 30%; background-color: rgb(14,10,90); margin-left: 35%; margin-bottom: 20px">Update</button>
                    </div>
                   
                </form>
            </div>
        </div>
    </div>

@endsection