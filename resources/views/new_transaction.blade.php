@extends('layouts.pagemaster')

@section('content')
<div class="page-wrapper">
<!-- Bread crumb -->
                @if (\Session::has('success'))
                
                <div class="alert alert-success col-md-12"  style="float:right" id="success_btn" >
                                <p>{!! \Session::get('success') !!}
                            <button type="submit" class="btn btn-primary mr-1" style="float:right" onclick="document.getElementById('success_btn').style.display = 'none'">
                                <i class="icon-check2"></i> Close</button></p>
                        </div>
                @endif
            <div class="row page-titles">
                <div class="col-md-5 align-self-center">
                    <h3 class="text-primary">New Transaction</h3> </div>
            </div>
            <!-- End Bread crumb -->
             <!-- Container fluid  -->
            <div class="container-fluid">
                <!-- Start Page Content -->
                <!-- /# row -->
                
                @if($transaction_type == "Withdrawal")
                <div class="row">
                    <div class="col-lg-12">
                        <div class="card">
                            <div class="card-title">
                                <h4>Withdrawal Information</h4>

                            </div>
                            <div class="card-body">
                                <div class="basic-elements">
                                    <form class="form" method="POST" action="{{ route('save_withdrawal') }}">
                                    @csrf
                                        <div class="row">
                                            <div class="col-lg-12">
                                                
                                                    <input type="text" hidden id="transaction_type" name="transaction_type" class="form-control" value="{{ $transaction_type }}">

                                                     

                                                <div class="form-group">
                                                    <input type="text" id="client_name" name="client_name" class="form-control @error('client_name') is-invalid @enderror" value="{{ old('client_name') }}" placeholder="Enter Client's name">

                                                     @error('client_name')
                                                        <span class="invalid-feedback" style="color:red" role="alert">
                                                            <strong>{{ $message }}</strong>
                                                        </span>
                                                    @enderror
                                                </div>

                                                <div class="form-group">
                                                    <input type="text" id="transaction_amount" name="transaction_amount" class="form-control @error('transaction_amount') is-invalid @enderror" value="{{ old('transaction_amount') }}" placeholder="Enter Transaction Amount">

                                                     @error('transaction_amount')
                                                        <span class="invalid-feedback" style="color:red" role="alert">
                                                            <strong>{{ $message }}</strong>
                                                        </span>
                                                    @enderror
                                                </div>

                                                <div class="form-group">
                                                    <input type="text" id="charges" name="charges" class="form-control @error('charges') is-invalid @enderror" value="{{ old('charges') }}" placeholder="Enter Charges">

                                                     @error('charges')
                                                        <span class="invalid-feedback" style="color:red" role="alert">
                                                            <strong>{{ $message }}</strong>
                                                        </span>
                                                    @enderror
                                                </div>

                                                 <div class="form-group">
                                                     <button type="submit"  class="btn btn-primary" style="margin-left:25%; width:150px">
                                                        <i class="icon-check2"></i> Save
                                                    </button>
                                                </div>
                                                
                                            </div>
                                           
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- /# column -->
                </div>
                <!-- /# row -->
                @endif

                @if($transaction_type == "Deposit")
                <div class="row">
                    <div class="col-lg-12">
                        <div class="card">
                            <div class="card-title">
                                <h4>Deposit Information</h4>

                            </div>
                            <div class="card-body">
                                <div class="basic-elements">
                                    <form class="form" method="POST" action="{{ route('save_Deposit') }}">
                                    @csrf
                                        <div class="row">
                                            <div class="col-lg-12">
                                                
                                                    <input type="text" hidden id="transaction_type" name="transaction_type" class="form-control" value="{{ $transaction_type }}">

                                                     

                                                <div class="form-group">
                                                    <input type="text" id="client_name" name="client_name" class="form-control @error('client_name') is-invalid @enderror" value="{{ old('client_name') }}" placeholder="Enter Client's name">

                                                     @error('client_name')
                                                        <span class="invalid-feedback" style="color:red" role="alert">
                                                            <strong>{{ $message }}</strong>
                                                        </span>
                                                    @enderror
                                                </div>

                                                <div class="form-group">
                                                    <input type="text" id="transaction_amount" name="transaction_amount" class="form-control @error('transaction_amount') is-invalid @enderror" value="{{ old('transaction_amount') }}" placeholder="Enter Transaction Amount">

                                                     @error('transaction_amount')
                                                        <span class="invalid-feedback" style="color:red" role="alert">
                                                            <strong>{{ $message }}</strong>
                                                        </span>
                                                    @enderror
                                                </div>

                                                <div class="form-group">
                                                    <input type="text" id="charges" name="charges" class="form-control @error('charges') is-invalid @enderror" value="{{ old('charges') }}" placeholder="Enter Charges">

                                                     @error('charges')
                                                        <span class="invalid-feedback" style="color:red" role="alert">
                                                            <strong>{{ $message }}</strong>
                                                        </span>
                                                    @enderror
                                                </div>

                                                 <div class="form-group">
                                                     <button type="submit"  class="btn btn-primary" style="margin-left:25%; width:150px">
                                                        <i class="icon-check2"></i> Save
                                                    </button>
                                                </div>
                                                
                                            </div>
                                           
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- /# column -->
                </div>
                <!-- /# row -->
                @endif




                @if($transaction_type == "Transfer")
                <div class="row">
                    <div class="col-lg-12">
                        <div class="card">
                            <div class="card-title">
                                <h4>Transfer Information</h4>

                            </div>
                            <div class="card-body">
                                <div class="basic-elements">
                                    <form class="form" method="POST" action="{{ route('save_transfer') }}">
                                    @csrf
                                        <div class="row">
                                            <div class="col-lg-12">
                                                
                                                    <input type="text" hidden id="transaction_type" name="transaction_type" class="form-control" value="{{ $transaction_type }}">

                                                     

                                                <div class="form-group">
                                                    <input type="text" id="client_name" name="client_name" class="form-control @error('client_name') is-invalid @enderror" value="{{ old('client_name') }}" placeholder="Enter Client's name">

                                                     @error('client_name')
                                                        <span class="invalid-feedback" style="color:red" role="alert">
                                                            <strong>{{ $message }}</strong>
                                                        </span>
                                                    @enderror
                                                </div>

                                                <div class="form-group">
                                                    <input type="text" id="transaction_amount" name="transaction_amount" class="form-control @error('transaction_amount') is-invalid @enderror" value="{{ old('transaction_amount') }}" placeholder="Enter Transaction Amount">

                                                     @error('transaction_amount')
                                                        <span class="invalid-feedback" style="color:red" role="alert">
                                                            <strong>{{ $message }}</strong>
                                                        </span>
                                                    @enderror
                                                </div>

                                                <div class="form-group">
                                                    <input type="text" id="charges" name="charges" class="form-control @error('charges') is-invalid @enderror" value="{{ old('charges') }}" placeholder="Enter Charges">

                                                     @error('charges')
                                                        <span class="invalid-feedback" style="color:red" role="alert">
                                                            <strong>{{ $message }}</strong>
                                                        </span>
                                                    @enderror
                                                </div>

                                                 <div class="form-group">
                                                     <button type="submit"  class="btn btn-primary" style="margin-left:25%; width:150px">
                                                        <i class="icon-check2"></i> Save
                                                    </button>
                                                </div>
                                                
                                            </div>
                                           
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- /# column -->
                </div>
                <!-- /# row -->
                @endif

                 @if($transaction_type == "Cable")
                <div class="row">
                    <div class="col-lg-12">
                        <div class="card">
                            <div class="card-title">
                                <h4>Cable/Electricity Subscription Information</h4>

                            </div>
                            <div class="card-body">
                                <div class="basic-elements">
                                    <form class="form" method="POST" action="{{ route('save_cable') }}">
                                    @csrf
                                        <div class="row">
                                            <div class="col-lg-12">
                                                
                                                    <input type="text" hidden id="transaction_type" name="transaction_type" class="form-control" value="{{ $transaction_type }}">

                                                     

                                                <div class="form-group">
                                                    <input type="text" id="client_name" name="client_name" class="form-control @error('client_name') is-invalid @enderror" value="{{ old('client_name') }}" placeholder="Enter Client's name">

                                                     @error('client_name')
                                                        <span class="invalid-feedback" style="color:red" role="alert">
                                                            <strong>{{ $message }}</strong>
                                                        </span>
                                                    @enderror
                                                </div>

                                                <div class="form-group">
                                                    <input type="text" id="transaction_amount" name="transaction_amount" class="form-control @error('transaction_amount') is-invalid @enderror" value="{{ old('transaction_amount') }}" placeholder="Enter Transaction Amount">

                                                     @error('transaction_amount')
                                                        <span class="invalid-feedback" style="color:red" role="alert">
                                                            <strong>{{ $message }}</strong>
                                                        </span>
                                                    @enderror
                                                </div>

                                                <div class="form-group">
                                                    <input type="text" id="charges" name="charges" class="form-control @error('charges') is-invalid @enderror" value="{{ old('charges') }}" placeholder="Enter Charges">

                                                     @error('charges')
                                                        <span class="invalid-feedback" style="color:red" role="alert">
                                                            <strong>{{ $message }}</strong>
                                                        </span>
                                                    @enderror
                                                </div>

                                                 <div class="form-group">
                                                     <button type="submit"  class="btn btn-primary" style="margin-left:25%; width:150px">
                                                        <i class="icon-check2"></i> Save
                                                    </button>
                                                </div>
                                                
                                            </div>
                                           
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- /# column -->
                </div>
                <!-- /# row -->
                @endif
                </div>
            </div>

@endsection