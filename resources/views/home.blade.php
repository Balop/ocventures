@extends('layouts.pagemaster')

@section('content')

        <div class="page-wrapper">
<!-- Bread crumb -->
            <div class="row page-titles">
                <div class="col-md-5 align-self-center">
                    <h3 class="text-primary">Dashboard</h3> </div>
            </div>
            <!-- End Bread crumb -->
             <!-- Container fluid  -->
            <div class="container-fluid">

                 @if (\Session::has('success'))
         <div class="alert alert-success col-md-12" id="success_btn" >
                    <p>{!! \Session::get('success') !!}
                <button type="submit" class="btn btn-primary mr-1" style="float: right" onclick="document.getElementById('success_btn').style.display = 'none'">
                    <i class="icon-check2"></i> Close</button></p>
            </div>
        @endif
                <!-- Start Page Content -->
                <div class="row">
                    <div class="col-md-12">
                        <div class="card bg-primary p-20">
                            <div class="media widget-ten">
                                <div class="media-left meida media-middle">
                                    <span>&#x20A6; {{ number_format($transaction_total, 2)}}</span>
                                </div>
                                <div class="media-body media-text-right">
                                    <h2 class="color-white">{{$transaction_count}}</h2>
                                    <p class="m-b-0">All Transactions</p>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-3">
                        <div class="card bg-pink p-20">
                            <div class="media widget-ten">
                                <div class="media-left meida media-middle">
                                    <span>&#x20A6; {{ number_format($withdrawal_total, 2)}}</span>
                                </div>
                                <div class="media-body media-text-right">
                                    <h2 class="color-white">{{ $withdrawal_count }}</h2>
                                    <p class="m-b-0">Withdrawals</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="card bg-success p-20">
                            <div class="media widget-ten">
                               <div class="media-left meida media-middle">
                                    <span>&#x20A6; {{ number_format($transfer_total, 2)}}</span>
                                </div>
                                <div class="media-body media-text-right">
                                    <h2 class="color-white">{{ $transfer_count }}</h2>
                                    <p class="m-b-0">Transfers</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="card bg-danger p-20">
                            <div class="media widget-ten">
                                <div class="media-left meida media-middle">
                                    <span>&#x20A6; {{ number_format($deposit_total, 2)}}</span>
                                </div>
                                <div class="media-body media-text-right">
                                    <h2 class="color-white">{{ $deposit_count }}</h2>
                                    <p class="m-b-0">Deposits</p>
                                </div>
                            </div>
                        </div>
                    </div>

                     <div class="col-md-3">
                        <div class="card bg-info p-20">
                            <div class="media widget-ten">
                                <div class="media-left meida media-middle">
                                    <span>&#x20A6; {{ number_format($cable_total, 2)}}</span>
                                </div>
                                <div class="media-body media-text-right">
                                    <h2 class="color-white">{{ $cable_count }}</h2>
                                    <p class="m-b-0">Cable/Electricity</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">
                   <div class="col-lg-12">
                        <div class="card">
                            <div class="card-title">
                                <h4>Most Recent Transactions</h4>
                            </div>
                            <div class="card-body">
                                <div class="table-responsive">
                                    <table class="table table-hover ">
                                        <thead>
                                            <tr>
                                                <th class="text-center" style="font-size: 10px;">Name</th>
                                                <th class="text-center" style="font-size: 10px;">Amount (&#x20A6;)</th>
                                                <th class="text-center" style="font-size: 10px;">Type</th>
                                                <th class="text-center" style="font-size: 10px;">Time</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @foreach($transactions as $key=> $transaction)
                                            <tr>
                                                <td class="text-center" style="font-size: 11px;">{{ $transaction->client_name }}</td>
                                                <td class="text-center" style="font-size: 11px;">{{ number_format($transaction->transaction_amount, 2) }}</td>
                                                <td class="text-center" style="font-size: 11px;">{{ $transaction->transaction_type }}</td>
                                                <td class="text-center" style="font-size: 11px;">{{ Carbon\Carbon::parse($transaction->created_at)->format('H:i:s') }}</td>
                                            </tr>
                                            @endforeach
                                           
                                        </tbody>
                                    </table>

                                   <!--  -->
                                </div>
                            </div>
                        </div>
                    </div>
                </div>



            <!-- container fluid has been closed below, so just keep adding all other section above this comment line  -->
            </div>
                <!-- end of container fluid -->

@endsection
