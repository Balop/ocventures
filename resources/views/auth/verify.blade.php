@extends('layouts.pagemaster')

@section('content')

<div class="error-box"> <div class="back-box"> <h2>Verify</h2> </div><div class="error-box-text"> <h1 style="font-size:20px ">
    @if (session('resent'))
                        <div class="alert alert-success" role="alert">
                            {{ __('A fresh verification link has been sent to your email address.') }}
                        </div>
                    @endif
                    
    Hello! {{Auth::User()->username}}</h1> <h3>Verify Your Email Address</h3> {{ __('Before proceeding, please check your email for a verification link.') }}
                    {{ __('If you did not receive the email') }}, <div class="mt-30"> <form class="d-inline" method="POST" action="{{ route('verification.resend') }}">
                        @csrf
                        <button type="submit" class="btn btn-link p-0 m-0 align-baseline">{{ __('click here to request another') }}</button>.
                    </form></div></div></div>

@endsection
