@extends('layouts.pagemaster')

@section('content')
<div class="page-title-section" style="background-image: url('{{('references/img/bg/aboutusBanner.png')}}');">
        <div class="container">
            <h1 style="float:left">Join Our Community</h1>
        </div>
    </div>
    @if (\Session::has('error'))
    <div class="alert alert-error col-md-4"  style="float:right" id="error_btn" >
                    <p>{!! \Session::get('error') !!}</p>
                <button type="submit" class="btn btn-primary mr-1" onclick="document.getElementById('error_btn').style.display = 'none'">
                    <i class="icon-check2"></i> Close</button>
            </div>
        @endif
    <div class="section-block" style="background-color:#F7F7F7;">
        <div class="container">
            <div class="row">
                <div class="col-md-6" style="background-color: black; border:none">
                  
                </div>
                <div class="col-md-6" style="background-color:black;  max-height:900px"">
                    <div class="rectangle-8" style="height:980px;margin-top:-5%">
                        <div class="pr-30-md">
                            <div class="section-heading center-holder">
                                <br /><br /><br /><br />
                                <span style="color:black">Register Now</span>
                                <h4 style="font-weight:900; color:#122335;">Join Our Platform Today</h4>
                                <h6 style="font-size:small">Join the Likemind Stakeholders Network Platform to build your Financial Status</h6>
                            </div>
                            <div class="mt-50" style="margin-left:2%">
                                <div class="contact-form-box">
                                    <form class="contact-form row"  method="POST" action="{{ route('reg') }}">
                                        @csrf


                                        <!-- input fields start here -->

                                        <div class="col-md-12">
                                            <input id="fname" type="text" class="form-control @error('fname') is-invalid @enderror" name="fname" value="{{ old('fname') }}" required autocomplete="fname" placeholder="First Name" autofocus>

                                            @error('fname')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                            @enderror

                                        </div>


                                        <div class="col-md-12">
                                            <input id="lname" type="text" class="form-control @error('lname') is-invalid @enderror" name="lname" value="{{ old('lname') }}" required autocomplete="lname" placeholder="Last Name" autofocus>

                                            @error('lname')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                            @enderror

                                        </div>


                                        <div class="col-md-12">
                                            <input id="username" type="text" class="form-control @error('username') is-invalid @enderror" name="username" value="{{ old('username') }}" required autocomplete="username" placeholder="Username" autofocus>

                                            @error('username')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                            @enderror
                                        </div>

                                        <div class="col-md-12">
                                            <input id="email" type="email" class="form-control col-md-12 @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" placeholder="Email" required autocomplete="email">

                                                @error('email')
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                @enderror
                                        </div>


                                        <div class="col-md-12">
                                            <input id="phone" type="phone" class="form-control col-md-12 @error('phone') is-invalid @enderror" name="phone" value="{{ old('phone') }}"  placeholder="Phone" required autocomplete="phone">

                                                @error('phone')
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                @enderror
                                        </div>

                                        <div class="col-md-12">
                                            <select name="plan" class="form-control @error('plan') is-invalid @enderror">
                                                <option selected="" disabled="disabled">--Select Plan--</option>
                                                <option value="Silver">Silver</option>
                                                <option value="Gold">Gold</option>
                                                <option value="Diamond">Diamond</option>
                                                <option value="Platinum">Platinum</option>
                                            </select>

                                            @error('plan')
                                                <span class="invalid-feedback" style="color:red" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                            @enderror
                                        </div>

                                        <div class="col-md-12">
                                            <input id="sponsor_id" type="sponsor_id" class="form-control col-md-12 @error('sponsor_id') is-invalid @enderror" name="sponsor_id" value="{{ old('sponsor_id') }}"  placeholder="Sponsor ID" autocomplete="sponsor_id">

                                                @error('sponsor_id')
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                @enderror
                                        </div>

                                        <!-- <div class="col-md-12">
                                            <input id="num" type="num" class="form-control col-md-12 @error('num') is-invalid @enderror" name="num" value="{{ old('num') }}"  placeholder="Num" autocomplete="num">

                                                @error('num')
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                @enderror
                                        </div>   
 -->
                                        <div class="col-md-12">
                                            <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" placeholder="Password" name="password" required autocomplete="new-password">

                                            @error('password')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                            @enderror
                                        </div>

                                        <div class="col-md-12">
                                            <input id="password-confirm" type="password" class="form-control" name="password_confirmation" placeholder="Confirm Password" required autocomplete="new-password">

                                            <br/><br/>
                                        </div>

                                        <div class="col-md-12">
                                                <button class="pricing-list-2-button pricing-list-2-button-act" style="float:left;background-color: blue; width: 30%">Register</button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>



                <!--<div class="col-md-5 col-sm-5 col-12 rectangle-8">

        </div>-->
                <div class="col-md-7 col-sm-7 col-12">

                </div>
            </div>
        </div>
    </div>

@endsection
