<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!-- Tell the browser to be responsive to screen width -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="Welcome to OC Money Fintech App. Seamless record management is the drive towards better customer service. Powered by Adlio Technologies">
    <meta name="author" content="Adlio Technologies">
    <!-- Favicon icon -->
    <link rel="icon" type="image/png" sizes="16x16" href="{{ asset('paga/images/favicon.png') }}">
    <title>OC Money Online Banking</title>

    <link href="{{ asset('paga/css/lib/chartist/chartist.min.css') }}" rel="stylesheet">
    <link href="{{ asset('paga/css/lib/owl.carousel.min.css') }}" rel="stylesheet" />
    <link href="{{ asset('paga/css/lib/owl.theme.default.min.css') }}" rel="stylesheet" />
    <!-- Bootstrap Core CSS -->
    <link href="{{ asset('paga/css/lib/bootstrap/bootstrap.min.css') }}" rel="stylesheet">
    <!-- Custom CSS -->
    <link href="{{ asset('paga/css/helper.css') }}" rel="stylesheet">
    <link href="{{ asset('paga/css/style.css') }}" rel="stylesheet">
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:** -->
    <!--[if lt IE 9]>matilda
    <script src="https:**oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js') }}"></script>
    <script src="https:**oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js') }}"></script>
<![endif]-->
</head>

<body class="fix-header fix-sidebar" style="background-image:url(paga/images/login1.jpg) ;
  /* Center and scale the image nicely */
  background-position: center;
  background-repeat: no-repeat;
  background-size: cover;">    <!-- Preloader - style you can find in spinners.css -->
    <div class="preloader">
        <svg class="circular" viewBox="25 25 50 50">
      <circle class="path" cx="50" cy="50" r="20" fill="none" stroke-width="2" stroke-miterlimit="10" /> </svg>
    </div>
    <!-- Main wrapper  -->
    <div id="main-wrapper">

        <div class="unix-login">
            <div class="container-fluid">
                <div class="row justify-content-center">
                    <div class="col-lg-4">
                        <div class="login-content card">
                            <div class="login-form">
                                <h4><strong style="font-size: 30px; font-style: italic;">O.C Money <i class="fa fa-bank"></i></strong></h4>
                                <form method="POST" action="{{ route('login') }}">
                                @csrf
                                    <div class="form-group">
                                        <label>Email address</label>
                                    <input id="email"placeholder="Email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus>

                                      @error('email')
                                          <span class="invalid-feedback" style="color: red" role="alert">
                                              <strong>{{ $message }}</strong>
                                          </span>
                                      @enderror
                                    </div>

                                    <div class="form-group">
                                        <label>Password</label>
                                         <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" placeholder="Password" name="password" required autocomplete="current-password">

                                @error('password')
                                    <span class="invalid-feedback" style="color: red" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                                    </div>
                                    <div class="checkbox">
                                        <label>
                           <input type="checkbox" value="lsRememberMe" id="rememberMe">&nbsp;<label  for="rememberMe">Remember me</label> 
                          </label><br>
                                        <label class="pull-right">
                             @if (Route::has('password.request'))
                                    <a   href="{{ route('password.request') }}">Forgot Password ?</a>
                                    @endif
                          </label>

                                    </div>
                                    <button type="submit" class="btn btn-primary btn-flat m-b-30 m-t-30">Sign in</button>
                                    <div class="register-link m-t-15 text-center">
                                        <p>Powered By <a href="http://belloadebowale.tk"> Adlio Technologies</a></p>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
    <!-- End Wrapper -->
    <!-- All Jquery -->
    <script src="paga/js/lib/jquery/jquery.min.js"></script>
    <!-- Bootstrap tether Core JavaScript -->
    <script src="paga/js/lib/bootstrap/js/popper.min.js"></script>
    <script src="paga/js/lib/bootstrap/js/bootstrap.min.js"></script>
    <!-- slimscrollbar scrollbar JavaScript -->
    <script src="paga/js/jquery.slimscroll.js"></script>
    <!--Menu sidebar -->
    <script src="paga/js/sidebarmenu.js"></script>
    <!--stickey kit -->
    <script src="paga/js/lib/sticky-kit-master/dist/sticky-kit.min.js"></script>
    <!--Custom JavaScript -->
    <script src="paga/js/custom.min.js"></script>

</body>

</html>