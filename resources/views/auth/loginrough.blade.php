@extends('layouts.pagemaster')

@section('content')
<div class="page-title-section" style="background-image: url('{{('references/img/bg/aboutusBanner.png')}}');">
        <div class="container">
            <h1 style="float:left">Login</h1>
        </div>
    </div>
             @if (\Session::has('success'))
                <div class="row">
                    <div class="alert alert-success col-md-4"  style="float:right" id="success_btn" >
                        <p>{!! \Session::get('success') !!}</p>
                            <button type="submit" class="btn btn-primary mr-1" onclick="document.getElementById('success_btn').style.display = 'none'">
                                    <i class="icon-check2"></i> Close
                            </button>
                    </div>
                </div>
            @endif
    <div class="section-block">
        <div class="container">
            <div class="row">
                <div class="col-md-6 col-sm-12 col-lg-6">
                    <img src="{{('references/img/bg/login_bg.png')}}">
                </div>
                <div class="col-md-6 col-sm-12 col-lg-6" style="margin-top: 7%">
                     <form method="POST" action="{{ route('login') }}" class="contact-form" style="max-width: 70%">
                       @csrf
                            <div class="row" style="">
                                <div class="col-md-12">
                                    <input id="email"placeholder="Email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus>

                                      @error('email')
                                          <span class="invalid-feedback" style="color: red" role="alert">
                                              <strong>{{ $message }}</strong>
                                          </span>
                                      @enderror
                                </div>

                                <div class="col-md-12">
                                   <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" placeholder="Password" name="password" required autocomplete="current-password">

                                @error('password')
                                    <span class="invalid-feedback" style="color: red" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
<!--               <span toggle="#password-field" class="fa fa-fw fa-eye field-icon toggle-password"></span>
 -->                                </div>
                                <div class="col-md-7">
                                   <label style="float:left;font-size:10px" for="rememberMe">Remember me</label><input type="checkbox" value="lsRememberMe" id="rememberMe"> 
                                </div>
                                 <div class="col-md-5" style="float:right">
                                     @if (Route::has('password.request'))
                                    <a style="color: yellow; font-size: 13px"  href="{{ route('password.request') }}">Forgot Password ?</a>
                                    @endif
                                </div>
                                <div class="col-md-12 mb-30">
                                    <div class="center-holder">
                                        <!-- <div > -->
                                            <button class="pricing-list-2-button pricing-list-2-button-act" style="float:left;background-color: blue; width: 30%">Login</button> <!-- style="font-size:small" onclick="lsRememberMe()"></a> -->
                                        <!-- </div> -->
                                    </div>
                                </div>
                                <p class="col-md-12 mb-30">Do not have an account yet? <br/>
                                    Please <a href="/join.html" style="color:#3F32D8;">Sign Up </a> here

                                </p>
                            </div>
                        </form>
                </div>
                </div>
            </div>
        </div>

 @endsection
 