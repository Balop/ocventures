@extends('layouts.pagemaster')

@section('content')
<div class="page-wrapper">
<!-- Bread crumb -->
            <div class="row page-titles">
                <div class="col-md-5 align-self-center">
                    <h3 class="text-primary">List Of Users</h3> </div>
            </div>

               @if (\Session::has('deleted'))
    <div class="alert alert-success col-md-12"  style="float:right" id="delete_btn" >
                    <p>{!! \Session::get('deleted') !!}
                <button type="submit" class="btn btn-primary mr-1" style="float:right" onclick="document.getElementById('delete_btn').style.display = 'none'">
                    <i class="icon-check2"></i> Close</button></p>
            </div>
        @endif
            <!-- End Bread crumb -->
             <!-- Container fluid  -->
            <div class="container-fluid">
                <!-- Start Page Content -->
                <div class="row">
                    <div class="col-lg-12">
                        <div class="card">
                            <div class="card-title">
                                <h4>List of users</h4>
                            </div>
                            <div class="card-body">
                                <div class="table-responsive">
                                    <table class="table table-hover ">
                                        <thead>
                                                           <tr>
                                                <th class="text-center">S/N</th>
                                                <th class="text-center">Name</th>
                                                <th class="text-center">Username</th>
                                                <th class="text-center">Email</th>

                                                @if(Auth::user()->user_role == 1)
                                                <th class="text-center" colspan="2">Action</th>
                                                @endif

                                            </tr>
                                           
                                        </thead>
                                        
                                        <tbody>
                                             @foreach($clients as $key=> $client)
                            <tr>
                                <td class="text-center">{{ $key + 1 }}</td>
                                <td class="text-center">{{ $client->name }}</td>
                                <td class="text-center">{{ $client->username }}</td>
                                <td class="text-center">{{ $client->email }}</span></td>
                                
                                 @if(Auth::user()->user_role == 1)
                              
                                <td class="text-center">
                                   <a href="{{route('deleteuser', $client->id)}}" class="btn-primary btn-sm"><i class="icon-trash"></i></a> 
                                </td>
                                @endif
                            </tr>
                            <!-- add edit modal -->
                            <!-- ./add edit modal -->
                            @endforeach

                                        </tbody>
                                    </table>

                                     @if(isset($clients))
                     <div style="margin-left: 20px">
                        {{$clients->links()}}
                    </div>
                    @endif   
                </div>
            </div>
        </div>
    </div>
</div>
<!-- /# row -->
</div>
</div>

@endsection