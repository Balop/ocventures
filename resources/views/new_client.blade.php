@extends('layouts.pagemaster')

@section('content')

<div class="app-content content container-fluid">
      <div class="content-wrapper">
        <div class="content-header row">
          <div class="content-header-left col-md-6 col-xs-12 mb-1">
          </div>
          @if (\Session::has('success'))
    <div class="alert alert-success col-md-12"  style="float:right" id="success_btn" >
                    <p>{!! \Session::get('success') !!}
                <button type="submit" class="btn btn-primary mr-1" style="float:right" onclick="document.getElementById('success_btn').style.display = 'none'">
                    <i class="icon-check2"></i> Close</button></p>
            </div>
        @endif
          <div class="content-header-right breadcrumbs-right breadcrumbs-top col-md-6 col-xs-12">
            <div class="breadcrumb-wrapper col-xs-12">
              <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{ route('home') }}">Home</a>
                </li>
                <li class="breadcrumb-item"><a href="#">New Client</a>
                </li>
              </ol>
            </div>
          </div>
        </div>
        <div class="content-body"><!-- Basic form layout section start -->
<section id="basic-form-layouts">
    <div class="row match-height">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    <h2 class="card-title" id="basic-layout-form">New Client Info</h2>
                    <a class="heading-elements-toggle"><i class="icon-ellipsis font-medium-3"></i></a>
                    <div class="heading-elements">
                        <ul class="list-inline mb-0">
                            <li><a data-action="collapse"><i class="icon-minus4"></i></a></li>
                            <li><a data-action="reload"><i class="icon-reload"></i></a></li>
                            <li><a data-action="expand"><i class="icon-expand2"></i></a></li>
                        </ul>
                    </div>
                </div>
                <div class="card-body collapse in">
                    <div class="card-block">
                        <div class="card-text" style="margin-left: 70%">
                         <form class="form" method="POST" action="{{ route('save') }}">
                            @csrf


                            <h4 class="form-section"><i class="icon-head form-control">&nbsp;Client</i>
                            <select name="ctype" class="form-control @error('ctype') is-invalid @enderror">
                                <option value="individual">Individual</option>
                                <option value="organization">Organization</option>
                            </select></h4>
                             
                             @error('ctype')
                                    <span class="invalid-feedback" style="color:red" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror

                        </div>
                            <div class="form-body">
                                <h4 class="form-section"><i class="icon-head"></i> Personal Info</h4>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="fname">First Name</label>
                                            <input type="text" id="fname" class="form-control @error('fname') is-invalid @enderror" value="{{ old('fname') }}" placeholder="First Name" name="fname">

                                             @error('fname')
                                    <span class="invalid-feedback" style="color:red" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                           <label for="lname">Last Name</label>
                                            <input type="text" id="lname" class="form-control @error('lname') is-invalid @enderror" name="lname" value="{{ old('lname') }}" placeholder="Last Name">

                                             @error('lname')
                                    <span class="invalid-feedback" style="color:red" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="email">E-email</label>
                                            <input type="eemail" id="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" placeholder="E-mail">

                                             @error('email')
                                    <span class="invalid-feedback" style="color:red" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="phone">Contact Number</label>
                                            <input type="number" id="phone" class="form-control @error('phone') is-invalid @enderror" name="phone" value="{{ old('phone') }}" placeholder="Phone No.">

                                             @error('phone')
                                    <span class="invalid-feedback" style="color:red" style="color:red" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                                        </div>
                                    </div>
                                </div>

                                <h4 class="form-section"><i class="icon-location"></i> Address</h4>

                                <div class="form-group">
                                    <label for="address">Company Address</label>
                                    <textarea cols="10" rows="4" id="address" class="form-control @error('address') is-invalid @enderror"  value="{{ old('address') }}"placeholder="Address" name="address"></textarea> 

                                     @error('address')
                                    <span class="invalid-feedback" style="color:red" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                                </div>

                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="state">State</label>
                                            <select id="state" name="state" class="form-control @error('state') is-invalid @enderror">
                                                <option value="0" selected="" disabled="disabled">-- Select State --</option>
                                                <option value="lagos">Lagos State</option>
                                                <option value="niger">Niger State</option>
                                            </select>

                                             @error('state')
                                    <span class="invalid-feedback" style="color:red" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                                        </div>
                                    </div>

                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="city">City</label>
                                            <select id="city" name="city" class="form-control @error('city') is-invalid @enderror">
                                                <option value="0" selected="" disabled="disabled">-- Select City --</option>
                                                <option value="island">Lagos Island</option>
                                                <option value="mainland">Lagos Main Land</option>
                                            </select>

                                            @error('city')
                                    <span class="invalid-feedback" style="color:red" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                                        </div>
                                    </div>

                                     <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="country">Country</label>
                                            <select id="country" name="country" class="form-control @error('city') is-invalid @enderror">
                                                 <option value="0" selected="" disabled="disabled">-- Select Country --</option>
                                                <option value="Nigeria">Nigeria</option>
                                                <option value="U.S.A">U.S.A</option>
                                                <option value="Spain">Spain</option>
                                                <option value="Brazil">Brazil</option>
                                            </select>

                                            @error('country')
                                    <span class="invalid-feedback" style="color:red" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                                        </div>
                                    </div>

                                      <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="postal_code">Postal Code</label>
                                            <input type="text" id="postal_code" class="form-control @error('phone') is-invalid @enderror" name="postal_code" value="{{ old('postal_code') }}" placeholder="E-mail">

                                             @error('postal_code')
                                    <span class="invalid-feedback" style="color:red" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <h4 class="form-section"><i class="icon-info"></i> Additional Information</h4>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="currency">Currency</label>
                                            <select id="currency" name="currency" class="form-control @error('currency') is-invalid @enderror">
                                                 <option value="0" selected="" disabled="disabled">-- Select Currency --</option>
                                                <option value="NGN">Nigerian Naira - NGN</option>
                                                <option value="USD">US Dollars - USD</option>
                                                <option value="GBP">British Pounds Sterling - GBP</option>
                                            </select>

                                             @error('currency')
                                    <span class="invalid-feedback" style="color:red" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                                        </div>
                                    </div>

                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="language">Language</label>
                                            <select id="language" name="language" class="form-control @error('language') is-invalid @enderror">
                                                <option value="0" selected="" disabled="">-- Select Language --</option>
                                                <option value="island">English</option>
                                                <option value="mainland">French</option>
                                            </select>

                                            @error('language')
                                    <span class="invalid-feedback" style="color:red" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                                        </div>
                                    </div>

                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="facebook">Facebook</label>
                                            <input type="text" id="facebook" class="form-control @error('facebook') is-invalid @enderror" name="facebook" value="{{ old('facebook') }}" placeholder="Facebook Name">

                                             @error('facebook')
                                    <span class="invalid-feedback" style="color:red" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                                        </div>
                                    </div>

                                      <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="instagram">Instagram</label>
                                            <input type="text" id="instagram" class="form-control @error('instagram') is-invalid @enderror" name="instagram" value="{{ old('instagram') }}" placeholder="Instagram Handle">

                                             @error('instagram')
                                    <span class="invalid-feedback" style="color:red" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                                        </div>
                                    </div>

                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="twitter">Twitter</label>
                                            <input type="text" id="twitter" class="form-control @error('twitter') is-invalid @enderror" name="twitter" value="{{ old('twitter') }}" placeholder="Twitter Handle">

                                             @error('twitter')
                                    <span class="invalid-feedback" style="color:red" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                                        </div>
                                    </div>

                                     <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="website_url">Website Url</label>
                                            <input type="text" id="website_url" class="form-control @error('website_url') is-invalid @enderror" name="website_url" value="{{ old('website_url') }}" placeholder="website url">

                                             @error('website_url')
                                    <span class="invalid-feedback" style="color:red" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                                        </div>
                                    </div>
                                </div>


                            <div class="form-actions">
                                <button type="submit" class="btn btn-primary mr-1">
                                    <i class="icon-check2"></i> Save
                                </button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
@endsection
