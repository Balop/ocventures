<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;



class Plan extends Model
// implements MustVerifyEmail
{

        use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name','amount',
    ]; 

     protected $hidden = [
        'created_at',
    ];
	


    public function user()
    {

    return $this->belongsTo(User::class);

    }
}
