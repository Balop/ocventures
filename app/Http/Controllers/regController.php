<?php

namespace App\Http\Controllers;
use App\User;
use App\Payment;
use App\Profile;
use Auth;
use Illuminate\Support\Facades\Hash;
use DB;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Session;


class regController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */

    public function user_register()
    {

      $person = User::findOrFail(Auth::user()->id);
    	return view('join',['person'=>$person,]);
    }

    public function save(Request $request){
        $validate = Validator::make($request->all(), [
            'name' => ['required', 'string', 'max:255'],
            'username' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'phone' => ['required', 'string', 'min:11', 'max:15'],
            // 'password' => ['required', 'string', 'min:8', 'confirmed'],


        ]);




                //validate
        if ($validate->fails()){
            return redirect()->back()
                        ->withErrors($validate)
                        ->withInput();
        }

        else{

   
       //populating user table
            $insert = User::create([

            'name' => $request['name'],
            'username' => $request['username'],
            'email' => $request['email'],
            'phone' => $request['phone'],
            'user_role' => 0,
            'password' => Hash::make('P@ssword2021'),
            'status' => 'active',
        ]);


         return redirect()->back()->with('success', 'User Profile Created Successfully');   


        }


}


  public function update_profile(Request $request)
      {
        $update = Profile::findOrFail($request->user_id); 
        $update->fname = $request->fname;
        $update->lname = $request->lname;
        $update->email = $request->email;
        $update->phone = $request->phone;
        $update->gender = $request->gender;
        $update->dob = $request->dob;
        $update->address = $request->address;


        $update->save();


             return redirect()->back()->with('success', 'Profile updated successfully');
             
      
     }



      public function editdetails(Request $request){
       $this->validate($request, [
            'username' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255'],
            'password' => ['required', 'string', 'min:8', 'confirmed'],


        ]);






        $object = User::findorfail(Auth::user()->id); 

        $object->username = $request->username;
        $object->password = Hash::make($request['password']);
        $object->email = $request->email;

        $object->save();



         return redirect()->back()->with('success', 'Proile updated successfully');   



}

}
