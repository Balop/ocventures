<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use App\User;
use App\Payment;
use Auth;
use DB;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;


class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = RouteServiceProvider::HOME;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'fname' => ['required', 'string', 'max:255'],
            'lname' => ['required', 'string', 'max:255'],
            'username' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'phone' => ['required', 'string', 'min:11', 'max:15'],
            'plan' => ['required', 'string', 'max:255'],
            'sponsor_id' => ['required', 'string', 'max:255'],
            // 'num' => ['required', 'string', 'max:225'],
            'password' => ['required', 'string', 'min:8', 'confirmed'],
        ]);
    }


    
    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    protected function create(array $data)
    {

  

               
             


        // $ref = rand(100000, 999999);
        // dd($ref);

               // This function will return a random 
// string of specified length 
function random_strings($length_of_string) 
{ 
  
    // String of all alphanumeric character 
    $str_result = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ'; 
  
    // Shufle the $str_result and returns substring 
    // of specified length 
    return substr(str_shuffle($str_result),  
                       0, $length_of_string); 
} 
  
// This function will generate 
// Random string of length 10 
$str_text = random_strings(2); 


   


// This function will return a random 
// string of specified length 
function random_number($length_of_string) 
{ 
  
    // String of all alphanumeric character 
    $str_result = '0123456789'; 
  
    // Shufle the $str_result and returns substring 
    // of specified length 
    return substr(str_shuffle($str_result),  
                       0, $length_of_string); 
} 
  
// This function will generate 
// Random string of length 10 

$str_no = random_number(8);
$refid = $str_text . $str_no;
$user_id = $str_text . $str_no;


//finding new user's sponsor network id
$network = DB::table('users')
              ->select('users.num')
              ->where('users.user_id', '=', $data['sponsor_id']) 
              ->first();

              $person = $network->num;


              $downlinecount = DB::table('users')
                ->where([
                    ['users.sponsor_id', '=', $data['sponsor_id'] ],
                    ['users.downline', '=', 'New'],
                     ])
                ->select('users.num')->count(); 

                

                // if($downlinecount > 1){

                //     echo "Cannot have more than 2";
                //     return redirect()->route('login'); 

                //     exit();
                // }

                 //            return view('auth.register')->with('error', 'Sponsor Already reached maximum downline');



        

        //populating payment table
        $save = Payment::create([
          'username' => $data['username'],
          'reference_id' => $refid,
          'status' => 'unpaid',
        ]);

        return User::create([

            'fname' => $data['fname'],
            'lname' => $data['lname'],
            'user_id' => $user_id,
            'state' => 'Active',
            'downline' => 'New',
            'username' => $data['username'],
            'email' => $data['email'],
            'phone' => $data['phone'],
            'plan' => $data['plan'],
            'sponsor_id' => $data['sponsor_id'],
            'num' => $person,
            'password' => Hash::make($data['password']),
        ]);
    }
}
