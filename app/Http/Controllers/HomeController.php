<?php

namespace App\Http\Controllers;
use App\User;
use App\Client;
use App\Transaction;
use App\Profile;
use App\Payment;
use Auth;
use DB;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Session;
use Illuminate\Support\Facades\Hash;


class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {

        // $client = DB::table('clients')
        // ->select('clients.*')
        // ->where()
        // ->orderby('created_at')
        // ->get();

        // $clientcount = DB::table('clients')
        // ->select('clients.*')->count();


        //to set user state as active

        // $update = User::findOrFail(Auth::User()->id); 
        //    $update->state = 'Active'; 
        //    $update->save();
       
        // // to get history payment of current user
        // $transactioncount = DB::table('payments')
        // ->where('payments.username', '=', Auth::user()->username)
        // ->select('payments.reference_id')->count();

        // // to populate nework  count and fetch network of current user
        // $networkcount = DB::table('users')
        // ->where([
        //     ['users.sponsor_id', '=', Auth::user()->user_id],
           
        //     ['users.fname', '<>', ''],
        //      ])
        // ->select('users.sponsor_id')->count();

            if(Auth::User()->user_role == 1){
              $person = User::where('username', '=', Auth::user()->username) 
              ->first();

               // to get the total amount and the whole transactions of all times
              $transaction_count = Transaction::all()
               ->count('id');

               // to get the total amount and the whole transactions of all times
              $transaction_total = Transaction::all()
               ->sum('transaction_amount');


              // to get the total amount and the whole transactions of all times
              $today_transaction_count = Transaction::where([['date', '=',  date('d-m-y')],])
               ->count('transaction_amount');



               $transactions = DB::table('transactions')
                ->where([
                    ['date', '=',  date('d-m-y')],
                    
                     ])
                ->select('transactions.*')
                ->orderBy('created_at', 'desc')
                ->take(10)
                ->get();




                // to get the total amount and the whole withdrawal transactions for the day
                $withdrawal_count = Transaction::where([

                  ['transaction_type', 'withdrawal'],

                  ['date', '=', date('d-m-y')],
                ])
                 ->count('transaction_amount');

                 $withdrawal_total = Transaction::where([

                  ['transaction_type', 'withdrawal'],

                  ['date', '=', date('d-m-y')],
                ])
                 ->sum('transaction_amount');



                // to get the total amount and the whole deposit transactions for the day
                $deposit_count = Transaction::where([

                  ['transaction_type', 'Deposit'],

                  ['date', '=', date('d-m-y')],
                ])
                 ->count('transaction_amount');

                 $deposit_total = Transaction::where([

                  ['transaction_type', 'Deposit'],

                  ['date', '=', date('d-m-y')],
                ])
                 ->sum('transaction_amount');


                // to get the total amount and the whole cable transactions for the day
                $cable_count = Transaction::where([

                  ['transaction_type', 'Cable'],

                  ['date', '=', date('d-m-y')],
                ])
                 ->count('transaction_amount');

                 $cable_total = Transaction::where([

                  ['transaction_type', 'Cable'],

                  ['date', '=', date('d-m-y')],
                ])
                 ->sum('transaction_amount');




                  // to get the total amount and the whole transfer transactions for the day
                $transfer_count = Transaction::where([

                  ['transaction_type', 'Transfer'],

                  ['date', '=', date('d-m-y')],
                ])
                 ->count('transaction_amount');

                 $transfer_total = Transaction::where([

                  ['transaction_type', 'Transfer'],

                  ['date', '=', date('d-m-y')],
                ])
                 ->sum('transaction_amount');

               }else{

                $person = User::where('username', '=', Auth::user()->username) 
              ->first();

               // to get the total amount and the whole transactions of all times
              $transaction_count = Transaction::where([

                  ['user_id', '=', Auth::User()->id],

                ])
               ->count('id');

               // to get the total amount and the whole transactions of all times
              $transaction_total = Transaction::where([

                  ['user_id', '=', Auth::User()->id],
                  
                ])
               ->sum('transaction_amount');


              // to get the total amount and the whole transactions of all times
              $today_transaction_count = Transaction::where([['date', '=',  date('d-m-y')],['user_id', '=', Auth::User()->id],])
               ->count('transaction_amount');



               $transactions = DB::table('transactions')
                ->where([
                    ['date', '=',  date('d-m-y')],

                    ['user_id', '=', Auth::User()->id],
                    
                     ])
                ->select('transactions.*')
                ->orderBy('created_at', 'desc')
                ->take(10)
                ->get();




                // to get the total amount and the whole withdrawal transactions for the day
                $withdrawal_count = Transaction::where([

                  ['transaction_type', 'withdrawal'],

                  ['date', '=', date('d-m-y')],

                  ['user_id', '=', Auth::User()->id],
                ])
                 ->count('transaction_amount');

                 $withdrawal_total = Transaction::where([

                  ['transaction_type', 'withdrawal'],

                  ['date', '=', date('d-m-y')],
                ])
                 ->sum('transaction_amount');



                  // to get the total amount and the whole deposit transactions for the day
                $deposit_count = Transaction::where([

                  ['transaction_type', 'Deposit'],

                  ['date', '=', date('d-m-y')],
                ])
                 ->count('transaction_amount');

                 $deposit_total = Transaction::where([

                  ['transaction_type', 'Deposit'],

                  ['date', '=', date('d-m-y')],

                  ['user_id', '=', Auth::User()->id],
                ])
                 ->sum('transaction_amount');




                  // to get the total amount and the whole transfer transactions for the day
                $transfer_count = Transaction::where([

                  ['transaction_type', 'Transfer'],

                  ['date', '=', date('d-m-y')],
                ])
                 ->count('transaction_amount');

                 $transfer_total = Transaction::where([

                  ['transaction_type', 'Transfer'],

                  ['date', '=', date('d-m-y')],

                  ['user_id', '=', Auth::User()->id],
                ])
                 ->sum('transaction_amount');


               }





              // dd($person);

        //       // dd($person);

        //       $plan = DB::table('users')
        //       ->join('plans','plans.name', '=', 'users.plan')
        //       ->select('plans.*')
        //       ->where('users.id', '=', Auth::user()->id) 
        //       ->first();



        //       // to fetch random networks detail to display
        //       // $network = User::inRandomOrder()->limit(5)->get()
        //       // ->where('num', '=', Auth::User()->num); // The amount of items you wish to receive

        //       $network = DB::table('users')
        //         ->where([
        //             ['users.sponsor_id', '=', Auth::user()->user_id],
        //             ['users.fname', '<>', ''],
        //              ])
        //         ->select('users.*')
        //         ->orderBy('fname', 'desc')
        //         ->limit(5)
        //         ->get();


        //     $current_network = DB::table('users')
        //         ->where([
        //             ['users.sponsor_id', '=', Auth::user()->user_id],
        //             ['users.downline', '=', 'New'],
        //             ['users.fname', '<>', ''],
        //              ])
        //         ->select('users.*')
        //         ->get();
        //       // dd($plan);


         return view('home', ['person' => $person, 'transaction_total' => $transaction_total, 'transaction_count' => $transaction_count, 'transactions' => $transactions,
          'withdrawal_total' => $withdrawal_total, 'withdrawal_count' => $withdrawal_count, 'deposit_total' => $deposit_total,
          'deposit_count' => $deposit_count, 'transfer_total' => $transfer_total, 'transfer_count' => $transfer_count, 'withdrawal_total' => $withdrawal_total, 'today_transaction_count' => $today_transaction_count, 'cable_total' => $cable_total, 'cable_count' => $cable_count,

       ]);

    }

     public function join()

    {

      return view('join');
         
    }

     public function profile()
    {

      return view('profile');
         
    }


    public function create_profile(Request $request){

            $validate = Validator::make($request->all(), [
            'fname' => ['required', 'string', 'max:255'],
            'lname' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:profiles'],
            'phone' => ['required', 'string', 'min:11', 'max:15'],
            'gender' => ['required', 'string', 'max:255'],
            'address' => ['required', 'string', 'max:255'],
            'dob' => ['required', 'string', 'max:255'],
            
        ]);





                //validate
        if ($validate->fails()){
            return redirect()->back()
                        ->withErrors($validate)
                        ->withInput();
        }

        //if all required fieds are filled as supposed then proceed to insert

        else{

        $verify = DB::table('users')
              ->select('users.*')
              ->where('users.id', '=', Auth::user()->id) 
              ->first();

        
        if ($verify->email <> $request->email || $verify->lname <> $request->lname || $verify->fname <> $request->fname){

            return redirect()->back()->with('success', 'Login credentials and profile values supplied does not match');            
         } 
        

        $obj = new Profile(); 

        $obj->user_id = Auth::user()->user_id;
        $obj->fname = $request->fname;
        $obj->lname = $request->lname;
        $obj->email = $request->email;
        $obj->phone = $request->phone;
        $obj->gender = $request->gender;
        $obj->dob = $request->dob;
        $obj->address = $request->address;
        
        $obj->save();

        // after inserting, update the value of profile_complete of user on the user table as yes
        $update = User::findOrFail(Auth::User()->id); 
        $update->profile_complete = 'yes'; 
        
        $update->save();

         return redirect('home')->with('success', 'You Information saved successfully');
    }

}



    public function edit_profile()
        {

        $profile = DB::table('users')
              ->join('profiles','profiles.user_id', '=', 'users.user_id')
              ->select('profiles.*')
              ->where('profiles.user_id', '=', Auth::user()->user_id) 
              ->first();

      return view('update_profile', ['profiles' => $profile,]);
         
    }


public function user_register()
     {
        
     return view('join');
     }

     public function network()
     {
             $update = User::findOrFail(Auth::User()->id); 
        $update->state = 'Active'; 
        $update->save();
       
        // to get history payment of current user
        $transactioncount = DB::table('payments')
        ->where('payments.username', '=', Auth::user()->username)
        ->select('payments.reference_id')->count();

        // to populate nework  count and fetch network of current user
        $networkcount = DB::table('users')
        ->where([
            ['users.sponsor_id', '=', Auth::user()->user_id],
            ['users.fname', '<>', ''],
             ])
        ->select('users.sponsor_id')->count();


             $person = DB::table('users')
              ->join('payments','payments.id', '=', 'users.id')
              ->select('payments.*','users.fname','users.lname','users.email','users.phone','users.plan')
              ->where('payments.id', '=', Auth::user()->id) 
              ->first();

              // dd($person);

              $plan = DB::table('users')
              ->join('plans','plans.name', '=', 'users.plan')
              ->select('plans.*')
              ->where('users.id', '=', Auth::user()->id) 
              ->first();



              // to fetch random networks detail to display
              // $network = User::inRandomOrder()->limit(5)->get()
              // ->where('num', '=', Auth::User()->num); // The amount of items you wish to receive

              $network = DB::table('users')
                ->where([
                    ['users.sponsor_id', '=', Auth::user()->user_id],
                    ['users.fname', '<>', ''],
                    ['users.downline', '<>', 'New'],
                     ])
                ->select('users.*')
                ->orderBy('created_at', 'desc')
                ->get();


            $current_network = DB::table('users')
                ->where([
                    ['users.sponsor_id', '=', Auth::user()->user_id],
                    ['users.downline', '=', 'New'],
                    ['users.fname', '<>', ''],
                     ])
                ->select('users.*')
                ->get();
              // dd($plan);


         return view('network', ['persons' => $person, 'plans'=> $plan,
           'transactioncount' => $transactioncount, 'networkcount' => $networkcount, 'networks' => $network, 'current_networks' => $current_network,]);
     }

     public function user_list()
    {


       $person = User::findOrFail(Auth::user()->id);

    $client = DB::table('users')
              ->where([
                    ['user_role', '<>', 1],
                    ['status', '<>', 'deleted'],
                     ])
            ->select('users.*')
            ->paginate(5); //this will show five records at a time with next option button


        return view('client_list', ['clients' => $client],['person' => $person]);

    
    }

    // if the transaction is withdrawal

     public function save_withdrawal(Request $request){
        $validate = Validator::make($request->all(), [
            'transaction_type' => ['required', 'string', 'max:225'],
            'client_name' => ['required', 'string', 'max:255'],
            'transaction_amount' => ['required', 'numeric'],
            'charges' => ['required', 'numeric'],
            

        ]);

                //validate
        if ($validate->fails()){
            return redirect()->back()
                        ->withErrors($validate)
                        ->withInput();
          }

          else{



          if($request->transaction_amount <= 19999){

            $profit = $request->charges - ((0.006)* $request->transaction_amount);  

          }else{

            $profit = $request->charges - 100;            
          }


        $obj = new Transaction(); 

        $obj->client_name = $request->client_name;
        $obj->transaction_type = $request->transaction_type;
        $obj->transaction_amount = $request->transaction_amount;
        $obj->charges = $request->charges;
        $obj->profit = $profit;
        $obj->date = date('d-m-y');
        $obj->user_id = Auth::User()->id;
        
        $obj->save();

         return redirect()->back()->with('success', 'Transaction Information saved successfully');   


        }

}






// if the transaction is Cable

     public function save_cable(Request $request){
        $validate = Validator::make($request->all(), [
            'transaction_type' => ['required', 'string', 'max:225'],
            'client_name' => ['required', 'string', 'max:255'],
            'transaction_amount' => ['required', 'numeric'],
            'charges' => ['required', 'numeric'],
            

        ]);

                //validate
        if ($validate->fails()){
            return redirect()->back()
                        ->withErrors($validate)
                        ->withInput();
          }

          else{
            


        $obj = new Transaction(); 

        $obj->client_name = $request->client_name;
        $obj->transaction_type = $request->transaction_type;
        $obj->transaction_amount = $request->transaction_amount;
        $obj->charges = $request->charges;
        $obj->profit = $request->charges;
        $obj->date = date('d-m-y');
        $obj->user_id = Auth::User()->id;
        
        $obj->save();

         return redirect()->back()->with('success', 'Transaction Information saved successfully');   


        }

}




// if the transaction is Deposit

     public function save_Deposit(Request $request){
        $validate = Validator::make($request->all(), [
            'transaction_type' => ['required', 'string', 'max:225'],
            'client_name' => ['required', 'string', 'max:255'],
            'transaction_amount' => ['required', 'numeric'],
            'charges' => ['required', 'numeric'],

        ]);

                //validate
        if ($validate->fails()){
            return redirect()->back()
                        ->withErrors($validate)
                        ->withInput();
        }

        else{



          if($request->transaction_amount <= 2000){

            $profit = $request->charges - 10;  

          }elseif( $request->transaction_amount <= 150000 &&  $request->transaction_amount > 2001){

            $profit = $request->charges - 20;            
          }
          else{
                $profit = $request->charges - 30;
            }


        $obj = new Transaction(); 

        $obj->client_name = $request->client_name;
        $obj->transaction_type = $request->transaction_type;
        $obj->transaction_amount = $request->transaction_amount;
        $obj->charges = $request->charges;
        $obj->profit = $profit;
        $obj->date = date('d-m-y');
        $obj->user_id = Auth::User()->id;
        
        $obj->save();

         return redirect()->back()->with('success', 'Transaction Information saved successfully');   


        }

}



// if the transaction is transfer
     public function save_transfer(Request $request){
        $validate = Validator::make($request->all(), [
            'transaction_type' => ['required', 'string', 'max:225'],
            'client_name' => ['required', 'string', 'max:255'],
            'transaction_amount' => ['required', 'numeric'],
            'charges' => ['required', 'numeric'],

        ]);

                //validate
        if ($validate->fails()){
            return redirect()->back()
                        ->withErrors($validate)
                        ->withInput();
        }

        else{

          $profit = $request->charges;

        $obj = new Transaction(); 

        $obj->client_name = $request->client_name;
        $obj->transaction_type = $request->transaction_type;
        $obj->transaction_amount = $request->transaction_amount;
        $obj->charges = $request->charges;
        $obj->profit = $profit;
        $obj->date = date('d-m-y');
        $obj->user_id = Auth::User()->id;
        
        $obj->save();

         return redirect()->back()->with('success', 'Transaction Information saved successfully');   


        }

}

  

      public function today_history(Request $request){

      $transaction_type = $request->transaction_type;


      // to get the total amount and the whole transactions for the day
      $transaction_total = Transaction::where([

        ['transaction_type', $transaction_type],

        ['date', '=', date('d-m-y')],
      ])
       ->sum('transaction_amount');


       // to get the total amount and the whole transactions for the day
      $profit_total = Transaction::where([

        ['transaction_type', $transaction_type],

        ['date', '=', date('d-m-y')],
      ])
       ->sum('profit');



      //  $transactions = Transaction::where([

      //   ['transaction_type', $transaction_type],

      //   ['date', '=', date('d-m-y')],
      // ])
      //  ->orderBy('created_at', 'desc')
      //  ->get();


                 if(Auth::User()->user_role == 1){

                 $transactions = DB::table('users')
                         ->join('transactions','transactions.user_id', '=', 'users.id')
                        ->select('users.name', 'transactions.*')
                        ->where([

                  ['transaction_type', $transaction_type],

                  ['date', '=', date('d-m-y')],

                  ['deleted', '<>', 'yes'],
                ])
                 ->orderBy('created_at', 'desc')
                 ->get();
               
               }else{

                    $transactions = DB::table('users')
                       ->join('transactions','transactions.user_id', '=', 'users.id')
                        ->select('users.name', 'transactions.*')
                        ->where([

                  ['transaction_type', $transaction_type],

                  ['date', '=', date('d-m-y')],

                  ['deleted', '<>', 'yes'],

                  ['transactions.user_id', '=', Auth::User()->id],
                ])
                 ->orderBy('created_at', 'desc')
                 ->get();

               }


   $person = User::findOrFail(Auth::user()->id);

  return view('today-history',['person'=>$person, 'transaction_type'=>$transaction_type, 'transactions' => $transactions, 'transaction_total' => $transaction_total, 'profit_total' => $profit_total]);   
   

  // Note: the code that follows below is useful too...but it is not as optimized as the one used above. it is an eloquent.

   // $person = DB::table('users')
       //  //       ->join('payments','payments.username', '=', 'users.username')
       //        ->select('users.*')
       //        ->where('username', '=', Auth::user()->username) 
       //        ->first(); 
      }




      public function all_today_history(Request $request){

      // to get the total amount and the whole transactions for the day
      $transaction_total = Transaction::where([

        ['date', '=', date('d-m-y')],

        ['deleted', '<>', 'yes'],

  
      ])
       ->sum('transaction_amount');


       // to get the total amount and the whole transactions for the day
      $profit_total = Transaction::where([

        ['date', '=', date('d-m-y')],

        ['deleted', '<>', 'yes'],

      ])
       ->sum('profit');



      //  $transactions = Transaction::where([

      //   ['transaction_type', $transaction_type],

      //   ['date', '=', date('d-m-y')],
      // ])
      //  ->orderBy('created_at', 'desc')
      //  ->get();


                 if(Auth::User()->user_role == 1){

                 $transactions = DB::table('users')
                         ->join('transactions','transactions.user_id', '=', 'users.id')
                        ->select('users.name', 'transactions.*')
                        ->where([

                  ['date', '=', date('d-m-y')],

                  ['deleted', '<>', 'yes'],

                ])
                 ->orderBy('created_at', 'desc')
                 ->get();
               
               }else{

                    $transactions = DB::table('users')
                       ->join('transactions','transactions.user_id', '=', 'users.id')
                        ->select('users.name', 'transactions.*')
                        ->where([

                  ['date', '=', date('d-m-y')],

                  ['deleted', '<>', 'yes'],

                  ['transactions.user_id', '=', Auth::User()->id],
                ])
                 ->orderBy('created_at', 'desc')
                 ->get();

               }


   $person = User::findOrFail(Auth::user()->id);

  return view('all_today_history',['person'=>$person, 'transactions' => $transactions, 'transaction_total' => $transaction_total, 'profit_total' => $profit_total]);   
   

  // Note: the code that follows below is useful too...but it is not as optimized as the one used above. it is an eloquent.

   // $person = DB::table('users')
       //  //       ->join('payments','payments.username', '=', 'users.username')
       //        ->select('users.*')
       //        ->where('username', '=', Auth::user()->username) 
       //        ->first(); 
      }


            public function all_history(Request $request){

      // to get the total amount and the whole transactions for the day
      $transaction_total = Transaction::where([
        ['deleted', '<>', 'yes'],

      ])
       ->sum('transaction_amount');


       // to get the total amount and the whole transactions for the day
      $profit_total = Transaction::where([
        ['deleted', '<>', 'yes'],

      ])
       ->sum('profit');



      //  $transactions = Transaction::where([

      //   ['transaction_type', $transaction_type],

      //   ['date', '=', date('d-m-y')],
      // ])
      //  ->orderBy('created_at', 'desc')
      //  ->get();


                 if(Auth::User()->user_role == 1){

                 $transactions = DB::table('users')
                         ->join('transactions','transactions.user_id', '=', 'users.id')
                        ->select('users.name', 'transactions.*')
                        ->where([
                                  ['deleted', '<>', 'yes'],

                                ])
                      
                 ->orderBy('created_at', 'desc')
                 ->get();
               
               }else{

                    $transactions = DB::table('users')
                       ->join('transactions','transactions.user_id', '=', 'users.id')
                        ->select('users.name', 'transactions.*')
                        ->where([


                  ['transactions.user_id', '=', Auth::User()->id],

                  ['deleted', '<>', 'yes'],

                ])
                 ->orderBy('created_at', 'desc')
                 ->get();

               }


   $person = User::findOrFail(Auth::user()->id);

  return view('all_history',['person'=>$person, 'transactions' => $transactions, 'transaction_total' => $transaction_total, 'profit_total' => $profit_total]);

}



  public function trash(Request $request){

      

                 $transactions = DB::table('users')
                         ->join('transactions','transactions.user_id', '=', 'users.id')
                        ->select('users.name', 'transactions.*')
                        ->where([
                                  ['deleted', '=', 'yes'],

                                ])
                      
                 ->orderBy('created_at', 'desc')
                 ->get();
               

   $person = User::findOrFail(Auth::user()->id);

  return view('trash',['person'=>$person, 'transactions' => $transactions,]);   
   

  // Note: the code that follows below is useful too...but it is not as optimized as the one used above. it is an eloquent.

   // $person = DB::table('users')
       //  //       ->join('payments','payments.username', '=', 'users.username')
       //        ->select('users.*')
       //        ->where('username', '=', Auth::user()->username) 
       //        ->first(); 
      }





    public function new_transaction(Request $request){

      $transaction_type = $request->transaction_type;


   $person = User::findOrFail(Auth::user()->id);

  return view('new_transaction',['person'=>$person, 'transaction_type'=>$transaction_type]);   
   

  // Note: the code that follows below is useful too...but it is not as optimized as the one used above. it is an eloquent.

   // $person = DB::table('users')
       //  //       ->join('payments','payments.username', '=', 'users.username')
       //        ->select('users.*')
       //        ->where('username', '=', Auth::user()->username) 
       //        ->first(); 
      }   



      public function deletetransaction(Request $request){

   $data = Transaction::findOrFail($request->id); 

   $data->deleted = 'yes'; 
    $data->save();  

         return redirect()->back()->with('deleted', 'Transaction information deleted successfully');   
      }

      public function restoretransaction(Request $request){

   $data = Transaction::findOrFail($request->id); 

   $data->deleted = 'no'; 
    $data->save();  

         return redirect()->back()->with('success', 'Transaction information restored successfully');   
      }  



       public function deleteuser(Request $request){

   $update = User::findOrFail($request->id);

   $update->status = 'deleted'; 
    $update->save();  

         return redirect()->back()->with('deleted', 'User information deleted successfully');   
      }  


       


public function paymentsuccessful(Request $request)
     {



      // i am writing two code sets here that both solves the same problem. so i will comment one out
      // $refid = $request->reference_id;
      //    $status = 'paid';

      // $update = Payment::findOrFail($refid); 
      // $update->status = $status; 
      // $update->save();


         $refid = $request->reference_id;
         $status = 'paid';
      
       Payment::where('reference_id', $refid)
       ->update([
           'status' => $status
        ]);


             return redirect('home')
             ->with('success', 'Payment For Chosen Plan Successful');
     }


         public function checkedit(Request $request)
    {

      $obj = User::findorfail($request->id);

          $data = Hash::check($request->password, $obj->password);

          
       if($data){

        



        return redirect()->route('edit_user');

       }

       else{

       return redirect()->back()->with('success', 'The password supplied is incorrect');
          
          }

      }


      public function edit_user()
    {

       $person = User::findorfail(Auth::user()->id);


        return view('edit_user', ['person' => $person,]);
        
    }


      public function deleteduser()
    {

        return view('deleteduser');
        
    }




}

?>