<?php

namespace App\Http\Middleware;

use Closure;
use Auth;
use DB;
use App\User;
use App\Profile;
use App\Payment;


class checkedit
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $profile = DB::table('users')
              ->join('profiles','profiles.user_id', '=', 'users.user_id')
              ->select('profiles.*','users.*')
              ->where('profiles.user_id', '=', Auth::user()->user_id) 
              ->first();
              
        if (!$profile){

             return redirect()->route('home')->with('success', 'You have not completed your profile yet');
          }
        return $next($request);
    }
}
