<?php

namespace App\Http\Middleware;

use Closure;
use Auth;
use DB;
use App\User;
use App\Payment;


class checkpaid
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $person = DB::table('users')
              ->join('payments','payments.id', '=', 'users.id')
              ->select('payments.*','users.fname','users.lname','users.email','users.phone','users.plan')
              ->where('payments.id', '=', Auth::user()->id) 
              ->first();
        if ($person->status == 'unpaid'){

             return redirect()->route('home')->with('success', 'You have to pay for your current before viewing network');
          }
        return $next($request);
    }
}


?>
