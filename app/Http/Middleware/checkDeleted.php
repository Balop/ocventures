<?php

namespace App\Http\Middleware;

use Closure;
use Auth;
use DB;
use App\User;
use App\Profile;
use App\Payment;


class checkDeleted
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $status = DB::table('users')
              ->select('users.*')
              ->where([
            ['users.id', '=', Auth::user()->id],
           
            ['users.status', '=', 'deleted'],
             ]) 
              ->first();
              
        if ($status){

             return redirect()->route('deleteduser')->with('error', 'User Profile Deleted');
          }
        return $next($request);
    }
}
