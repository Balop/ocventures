<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;



class Client extends Model 
// implements MustVerifyEmail
{

        use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'client_type','first_name', 'last_name', 'email', 'website_url', 'address', 'postal_code',
         'state', 'city', 'country', 'phone', 'facebook',  'instagram', 'twitter', 
         'currency', 'language',
    ];  

    public function user()
    {

    return $this->belongsTo(User::class);

    }
}
