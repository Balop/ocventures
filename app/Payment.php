<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;



class Payment extends Model
// implements MustVerifyEmail
{

        use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'reference_id','username','status',
    ]; 

     protected $hidden = [
        'created_at',
    ];
	


    public function user()
    {

    return $this->belongsTo(User::class);

    }
}
