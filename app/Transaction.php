<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;



class Transaction extends Model
// implements MustVerifyEmail
{

        use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'client_name','transaction_type', 'transaction_amount', 'charges', 'profit', 'date', 'user_id', 'deleted'
    ]; 

     protected $hidden = [
        'created_at',
    ];
	


    public function user()
    {

    return $this->belongsTo(User::class);

    }
}
