<?php

use Illuminate\Support\Facades\Route;
use App\User;


/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});



Auth::routes();

Route::get('/home', 'HomeController@index')->name('home')->middleware(['verified','checkDeleted',]);

Route::get('/join', 'regController@user_register')->name('join');


Route::post('/reg', 'regController@save')->name('reg');

Route::post('/editdetails', 'regController@editdetails')->name('editdetails')->middleware(['verified','checkDeleted',]);

// in the below route, i am saving data, so i am using post because i am submitting data via a form to save data

Route::post('/xyy', 'HomeController@save_withdrawal')->name('save_withdrawal')->middleware(['verified','checkDeleted',]);

Route::post('/xy', 'HomeController@save_Deposit')->name('save_Deposit')->middleware(['verified','checkDeleted',]);

Route::post('/xyx', 'HomeController@save_transfer')->name('save_transfer')->middleware(['verified','checkDeleted',]);

Route::post('/xyz', 'HomeController@save_cable')->name('save_cable')->middleware(['verified','checkDeleted',]);

Route::get('/new_transaction/{transaction_type}', 'HomeController@new_transaction')->name('new_transaction')->middleware(['verified','checkDeleted',]);

Route::get('/today-history/{transaction_type}', 'HomeController@today_history')->name('today_history')->middleware(['verified','checkDeleted',]);

Route::get('/all-today-history', 'HomeController@all_today_history')->name('all_today_history')->middleware(['verified','checkDeleted',]);

Route::get('/all-history', 'HomeController@all_history')->name('all_history')->middleware(['verified','checkDeleted',]);

Route::get('/trash', 'HomeController@trash')->name('trash')->middleware(['verified','checkDeleted',]);

//in the below commented route i am using a get because i will be getting data attached to the url 

Route::get('/deletetransaction/{id}', 'HomeController@deletetransaction')->name('deletetransaction')->middleware(['verified','checkDeleted',]);

Route::get('/restoretransaction/{id}', 'HomeController@restoretransaction')->name('restoretransaction')->middleware(['verified','checkDeleted',]);

Route::get('/deleteuser/{id}', 'HomeController@deleteuser')->name('deleteuser')->middleware(['verified','checkDeleted',]);

Route::get('/user_list', 'HomeController@user_list')->name('user_list')->middleware(['verified','checkDeleted',]);

Route::get('/deleteduser', 'HomeController@deleteduser')->name('deleteduser')->middleware(['verified',]);

Route::post('/checkedit', 'HomeController@checkedit')->name('checkedit')->middleware(['verified','checkDeleted',]);

Route::get('/edit_user', 'HomeController@edit_user')->name('edit_user')->middleware(['verified','checkDeleted',]);

Route::post('/update_profile/{user_id}', 'regController@update_profile')->name('update_profile')->middleware(['verified','checkDeleted',]);

Route::get('/paymentsuccessful', 'HomeController@paymentsuccessful')->name('paymentsuccessful')->middleware(['verified','checkDeleted',]);

Route::get('/network', 'HomeController@network')->name('network')->middleware(['verified','checkpaid','checkedit', 'checkDeleted',]);

Route::get('/profile', 'HomeController@profile')->name('profile')->middleware(['verified', 'checkpaid', 'checkprofile', 'checkDeleted',]);

Route::post('/create_profile', 'HomeController@create_profile')->name('create_profile')->middleware(['verified','checkDeleted',]);

Route::get('/edit_profile', 'HomeController@edit_profile')->name('edit_profile')->middleware(['verified','checkpaid','checkedit', 'checkDeleted',]);

 Route::get('/logout', function() {
 Session::forget('key');

     // $update = User::findOrFail(Auth::User()->id); 
     //    $update->state = 'Offline'; 
     //   	$update->save();
       	
    Auth::logout();
    Session::flush();

    
    return redirect('login');
 });

Auth::routes(['verify' => true]);

